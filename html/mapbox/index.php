<?php require_once 'simplexlsx.class.php';?>
<html>
<head>
    <meta charset='utf-8' />
    <title>Add live realtime data</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.js'></script>
    <script src="js/jquery-3.2.1.slim.min.js"></script>
    <script src="js/moment.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>

    <link href='https://api.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css' rel='stylesheet'/>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    
    <style>
        body { margin:0; padding:0; }
        #map { position: absolute; bottom: 0; left: 0; width: 100%; height: calc(100vh - 150px); }
        .map-form-wrap{ position: relative; z-index: 99; background: rgba(255, 255, 255, 0.7); width: 100%; padding: 0px 15px; left: 15px; top: 15px; }
        .map-form-wrap .load-map-btn { margin-top:31px; }
        .map-form-wrap .logo{ max-width: 50%; height: auto; }
    </style>
    <script>
        $(function () {
            $('#fromdatepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
            $('#todatepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });
    </script>
</head>
<body>
<?php 
    if(isset($_POST['fromdatepicker'])) {
        $fromdate = strtotime($_POST['fromdatepicker']);
    } else {
        $fromdate = '1509494400';
    } 

    if(isset($_POST['toatepicker'])) {
        $todate = strtotime($_POST['toatepicker']);
    } else {
        $todate = time();
    }
    if(!isset($_POST['mapParameters'])) {
        $map_parameters = 'visitors';
        $map_description = 'Visitors';
    } else {
        $map_parameters = $_POST['mapParameters'];
        if($map_parameters == 'frequency') {
            $map_description = 'Frequency';
        } else if($map_parameters == 'avg_duration') {
            $map_description = 'Average Duration';
        } else if($map_parameters == 'count') {
            $map_description = 'Count';
        } else if($map_parameters == 'visitors') {
            $map_description = 'Visitors';
        }
    }

    $curl = curl_init();
    //echo "https://cloud.estimote.com/v2/analytics/visits/total?from=".$fromdate."&to=".$todate."&granularity=monthly&group_by=tag";die;
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://cloud.estimote.com/v2/analytics/visits/total?from=".$fromdate."&to=".$todate."&granularity=monthly&group_by=tag",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Authorization: Basic aW9zLTF3ZjpkYWYyNDgxMTM0ZDI5ZGIzYWI0ZThjZTY5ODZmN2ZkMA=="
        ),
    ));

    $response_json = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;die;
    } else {
        $result = json_decode($response_json);
        $resultFromAPI = $result->data;
    }

    $data = array();
    $resultData = array();
    if ( $xlsx = SimpleXLSX::parse('Final_Beacon_Conf-2.xlsx')) {
        list( $cols, ) = $xlsx->dimension();
        $i = 0;   
		foreach ( $xlsx->rows() as $k => $r ) {
            if ($k == 0) continue; // skip first row
            if ($k == 1) continue; // skip second row
            if($k == 2)  continue; // skip third row
            if(is_int($r[12])) {
                $data[$r[12]]['Longitude'] = $r[5];
                $data[$r[12]]['Latitude'] = $r[6];
                $data[$r[12]]['tag'] = $r[11];
            }
            $i++;
		}
    } else {
        echo SimpleXLSX::parse_error();
    } 

    if(is_array($resultFromAPI)) {
        $resultData['type'] = 'FeatureCollection';
        $resultData['crs']['type'] = 'name';
        $resultData['crs']['properties']['name'] = 'urn:ogc:def:crs:OGC:1.3:CRS84';
        $j = 0;
        for($i=0;$i<count($resultFromAPI);$i++) {
            if(array_key_exists($resultFromAPI[$i]->tag_id, $data)) {
                $resultData['features'][$j]['type'] = 'Feature';
                $resultData['features'][$j]['properties']['id'] = $resultFromAPI[$i]->tag_id;
                $resultData['features'][$j]['properties']['mag'] = $resultFromAPI[$i]->frequency;
                $resultData['features'][$j]['properties']['time'] = '1507425650893';
                $resultData['features'][$j]['properties']['title'] = 'Location: '.$data[$resultFromAPI[$i]->tag_id]['tag'];
                $resultData['features'][$j]['properties']['description'] = $map_description.': '.$resultFromAPI[$i]->$map_parameters;
                $resultData['features'][$j]['geometry']['type'] = 'Point';
                $resultData['features'][$j]['geometry']['coordinates'] = [$data[$resultFromAPI[$i]->tag_id]['Longitude'],$data[$resultFromAPI[$i]->tag_id]['Latitude']];
                $j++;
            }
        }
    } 
    $result = json_encode($resultData);
?>

<div class="container">
    <form class="map-form-wrap" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="row">
            <div class='col-3'>
                <div class="form-group">
                    <label>From: </label>
                    <input type="text" class="form-control" id="fromdatepicker" name="fromdatepicker" value="<?php if(isset($_POST['fromdatepicker'])) { echo $_POST['fromdatepicker']; }?>">
                </div>
            </div>
            <div class='col-3'>
                <div class="form-group">
                    <label>To: </label>
                    <input type="text" class="form-control" id="todatepicker" name="todatepicker" value="<?php if(isset($_POST['todatepicker'])) { echo $_POST['todatepicker']; }?>">
                </div>
            </div>
            <div class='col-2'>
                <div class="form-group">
                    <label for="sel1">Type (select one):</label>
                    <select class="form-control" id="sel1" name="mapParameters">
                        <option value="frequency" <?php if($map_parameters == 'frequency') { echo 'selected'; } ?> >Frequency</option>
                        <option value="avg_duration" <?php if($map_parameters == 'avg_duration') { echo 'selected'; } ?>>Average Duration</option>
                        <option value="count" <?php if($map_parameters == 'count') { echo 'selected'; } ?>>Count</option>
                        <option value="visitors" <?php if($map_parameters == 'visitors') { echo 'selected'; } ?>>Visitors</option>
                    </select>
                </div>
            </div>
            <div class="col-2">
                <button class="btn btn-primary load-map-btn" type="submit" name="submit-btn" value="loadmap">Load Map</button>
            </div>
            <div class="col-2">
                <img src="images/Al-Ain-Zoo.jpg" class="logo">
            </div>
        </div>
    </form>
    <div class='cal-sm-12' >
        <div id='map' ></div>
    </div>
</div>

<script>
    var geojson = <?php echo $result; ?>;

    mapboxgl.accessToken = 'pk.eyJ1IjoibW9obG9heSIsImEiOiJjajZodG9wazcwbHprMndueTFsaGlwbGVkIn0.bYKN4ejyhP2l4VXy3HPgHg';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/dark-v9',
        center: [55.738492, 24.1760813],
        zoom: 15
    });

    map.on('load', function() {
        // Add a geojson point source.
        // Heatmap layers also work with a vector tile source.
        map.addSource('earthquakes', {
            "type": "geojson",
            "data": geojson,
            "cluster": true,
            "clusterMaxZoom": 14, // Max zoom to cluster points on
            "clusterRadius": 30
        });

        map.addLayer({
            id: "clusters",
            type: "circle",
            source: "earthquakes",
            filter: ["has", "point_count"],
            paint: {
                // Use step expressions (https://www.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
                // with three steps to implement three types of circles:
                //   * Blue, 20px circles when point count is less than 100
                //   * Yellow, 30px circles when point count is between 100 and 750
                //   * Pink, 40px circles when point count is greater than or equal to 750
                "circle-color": [
                    "step",
                    ["get", "point_count"],
                    "#51bbd6",
                    2,
                    "#f1f075",
                    5,
                    "#f28cb1",
                    10,
                    "#C26C20"
                ],
                "circle-radius": [
                    "step",
                    ["get", "point_count"],
                    10,
                    50,
                    15,
                    100,
                    20
                ]
            }
        });

        map.addLayer({
            id: "cluster-count",
            type: "symbol",
            source: "earthquakes",
            filter: ["has", "point_count"],
            layout: {
                "text-field": "{point_count_abbreviated}",
                "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
                "text-size": 10
            }
        });

        map.addLayer({
            "id": "earthquakes-point",
            "type": "circle",
            "source": "earthquakes",
            "minzoom": 7,
            "paint": {
                // Size circle radius by earthquake magnitude and zoom level
                "circle-radius": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    7, [
                        "interpolate",
                        ["linear"],
                        ["get", "mag"],
                        1, 1,
                        6, 4
                    ],
                    16, [
                        "interpolate",
                        ["linear"],
                        ["get", "mag"],
                        1, 4,
                        8, 20
                    ]
                ],
                // Color circle by earthquake magnitude
                "circle-color": [
                    "interpolate",
                    ["linear"],
                    ["get", "mag"],
                    1, "rgba(33,102,172,0)",
                    2, "rgb(103,169,207)",
                    3, "rgb(209,229,240)",
                    4, "rgb(253,219,199)",
                    5, "rgb(239,138,98)",
                    6, "rgb(178,24,43)"
                ],
                "circle-stroke-color": "white",
                "circle-stroke-width": 1,
                // Transition from heatmap to circle layer by zoom level
                "circle-opacity": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    7, 0,
                    8, 1
                ]
            }
        }, 'waterway-label');

        map.on('click', 'earthquakes-point', function(e) {
            if(e.features[0].properties.title != undefined || e.features[0].properties.description != undefined) {
                new mapboxgl.Popup()
                    .setLngLat(e.features[0].geometry.coordinates)
                    .setHTML(e.features[0].properties.title + '<br/>' + e.features[0].properties.description)
                    .addTo(map);
            }
        });

        map.addLayer({
            "id": "earthquakes-heat",
            "type": "heatmap",
            "source": "earthquakes",
            "maxzoom": 9,
            "paint": {
                // Increase the heatmap weight based on frequency and property magnitude
                "heatmap-weight": [
                    "interpolate",
                    ["linear"],
                    ["get", "mag"],
                    0, 0,
                    6, 1
                ],
                // Increase the heatmap color weight weight by zoom level
                // heatmap-intensity is a multiplier on top of heatmap-weight
                "heatmap-intensity": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    0, 1,
                    9, 3
                ],
                // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
                // Begin color ramp at 0-stop with a 0-transparancy color
                // to create a blur-like effect.
                "heatmap-color": [
                    "interpolate",
                    ["linear"],
                    ["heatmap-density"],
                    0, "rgba(33,102,172,0)",
                    0.2, "rgb(103,169,207)",
                    0.4, "rgb(209,229,240)",
                    0.6, "rgb(253,219,199)",
                    0.8, "rgb(239,138,98)",
                    1, "rgb(178,24,43)"
                ],
                // Adjust the heatmap radius by zoom level
                "heatmap-radius": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    0, 15,
                    9, 15
                ],
                // Transition from heatmap to circle layer by zoom level
                "heatmap-opacity": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    7, 5,
                    9, 0
                ],
            }
        }, 'waterway-label');

        // map.addLayer({
        //     id: "unclustered-point",
        //     type: "circle",
        //     source: "earthquakes",
        //     filter: ["!has", "point_count"],
        //     paint: {
        //         "circle-color": "#11b4da",
        //         "circle-radius": 4,
        //         "circle-stroke-width": 1,
        //         "circle-stroke-color": "#fff",
        //     }
        // });     

    });
    
</script>

</body>
</html>

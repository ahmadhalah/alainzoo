/**
 * @file Behaviors for the Ain theme.
 */

(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.ainTheme = {
    attach: function (context, settings) {

      var pollView = $('.view-display-id-latest_poll');
      if (pollView.length) {
        pollView.trigger('RefreshView');
      }

      $(".accordion-title", context).click(function () {
        $(this).parent('.group-wrapper').find('.accordion-wrapper').toggle(this);
      });

      $(".views-field-title", context).click(function () {
        $(this).parent('.views-row').find('.views-field-body').slideToggle('slow', function () {
        });
      });

      $('.navbar-toggle').click(function () {
        $('body').addClass('show-menu');
        $('.mobile-overlay').show();
        $('.mobile-menu-outer-wrapper').animate({
          width: 300
        });
        var top;
        if (('#toolbar-bar').length) {
          top = $('#toolbar-bar').height();
        } else {
          top = 0;
        }

        $('.mobile-menu-outer-wrapper').css({'top': top + 'px'});
      });

      $('.mobile-overlay').click(function () {
        $(this).hide();
        $('body').removeClass('show-menu');
//        $('.mobile-menu-outer-wrapper').hide();
        $('.mobile-menu-outer-wrapper').animate({
          width: 0
        });
      });

      $('.field--name-dynamic-token-fieldnode-enquire-link a').click(function (event) {
        event.preventDefault();
        var node = $(this).attr('value');
        $('#webform-submission-special-occasions-node-908-add-form #edit-service-name').val(node);
        $('html, body').animate({
          scrollTop: $($(this).attr('href')).offset().top
        }, 500);
        return false;
      });

      $('.node--type-article.node--view-mode-full .field--name-field-images, .node--type-zoolife.node--view-mode-full .field--name-field-images').slick({
        slidesToShow: 4,
        slidesToScroll: 2,
        arrows: false,
        dots: true,
        rtl: drupalSettings.path.currentLanguage === 'ar',
        infinite: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
      $('.node--type-article.node--view-mode-full .field--name-field-images img, .node--type-zoolife.node--view-mode-full .field--name-field-images img').colorbox({
        href: function () {
          console.log($(this).attr('src'))
          return $(this).attr('src');
        },
        innerWidth: '80%',
        scalePhotos: true,
        innerHeight: '80%',
        rel: 'photo',
      })
      $('#popup .node--type-popup.node--view-mode-teaser .field--name-body a', context).on('click', function () {
        const id = $('#popup .node--type-popup.node--view-mode-teaser .field--name-node-title h2').text();
        ga('send', 'event', 'Popup', 'clicked', id, 1);
      });

      $('.view-id-gallery_tabs.view-display-id-gallery_image .node--type-gallery img').colorbox({
        href: function () {
        return $(this).attr('src');
        },
        rel: 'group1',
        title: false,
        innerWidth: '80%',
        scalePhotos: true,
        innerHeight: '80%'

      });

      $('.view-id-gallery_tabs.view-display-id-gallery_video .node--type-gallery .field--name-field-media-oembed-video').colorbox({
        href: function (e) {
          return $(this).find('iframe').attr('src');
        },
        rel: 'video',
        title: false,
        innerWidth: '80%',
        innerHeight: '80%'
      });
      $('.view-id-gallery_tabs.view-display-id-gallery_video .node--type-gallery .field--name-field-media-video-file-1').colorbox({
        href: function (e) {
          return `${window.location.origin}/${$(this).find('video source').attr('src')}`;
        },
        rel: 'video',
        title: false,
        innerWidth: '80%',
        innerHeight: '80%',
        iframe: true,
      });
      document.addEventListener('play', function(e){
        var audios = document.querySelectorAll('audio');
        for(var i = 0, len = audios.length; i < len;i++){
          if(audios[i] != e.target){
            audios[i].pause();
          }
        }
      }, true);

      // This line for visit plan modal
      $('.visit-plan-modal').on('click', () => {
        $(this).dialog({height:'auto', width:'auto'});
      })
    }
  };

//  Drupal.behaviors.vimeoFroogaloop = {
//    attach: function (context, settings) {
//      $(window).on('load', function () {
//        if ($('.video-responsive > iframe', context).length && !$('.video-responsive > .video-overlay', context).length) {
//          $('.video-responsive').append('<div class="video-overlay"></div>');
//
//          var iframe = $('.video-responsive > iframe')[0];
//          var player = $f(iframe);
//
//          player.api('play');
//
//          setTimeout(function () {
//            player.api('play');
//          }, 750);
//        }
//      });
//    }
//  }

  //search input


  $("#views-exposed-form-search-search .form-inline .radio input").on("click", function () {
    $("#views-exposed-form-search-search .form-inline .radio input").removeAttr('checked');
    document.getElementById("views-exposed-form-search-search").submit();
    $('#views-exposed-form-search-search label').removeClass('checkedone');
    $(this).parents('label').addClass('checkedone');
    $(this).attr('checked', 'checked');
  });
////


  $(".mobile-search").click(function () {
    $("#block-exposedformsearchsearch-3, .blocksearch-search-mobile").slideToggle('slow', function () {
    });
  });

  $('table').addClass('table');
  $('table').wrap("<div class='table-responsive'></div>");

  $(document).ready(function () {
    $('.fancybox-media').fancybox({
      openEffect: 'none',
      closeEffect: 'none',
      helpers: {
        media: {}
      }
    });

    if ($('#block-usermenu .menu li a.help').length) {
      var startTour = $('#block-usermenu .menu li a.help');
      startTour.click(function (event) {
        event.preventDefault();
        var tourObject = drupalSettings.bs_tour.currentTour;
        if (tourObject && tourObject._options.steps.length) {
          tourObject.start(true);
          tourObject.goTo(0);
        }
      });
    }

    $(".form-inline .radio input:checked").parents('label').addClass('checkedone');

    var popid = $('#popup .views-row').attr('class');
    if (popid) {
      popid = popid.substring(9, popid.indexOf(' '));
    }

    if (localStorage.popupOpened == null || localStorage.popupOpened != popid) {
      localStorage.setItem("popupOpened", popid);
      $("#open-popup").trigger('click');
    }

    //SZDLC Form
    $(".szdlc-webform , .enquire-about-the-service").wrap("<div class='page-with-form'></div>").wrap("<div class='lh-row-11'></div>");
    $(".szdlc-webform , .enquire-about-the-service").children().wrapAll("<div class='row'></div>");
  });

  $(".form-item-field-avatar .views-field-field-avatar img").click(function () {
    if ($(this).parent('label').find('.form-radio').prop("checked", false)) {
      $(this).parent('label').find('input').prop("checked", true);
    }
  });

  function videoHeader() {
    var iframeHeight = screen.height;
    var iframeWidth = iframeHeight * 1.77
    if ($('.header-full-video iframe').length) {
      $('.header-full-video iframe').height(iframeHeight + "px");
      $('.header-full-video iframe').width(iframeWidth + "px")
    }
  }

  videoHeader();

  $(window).resize(function () {
    videoHeader();
  })

  // $(window).on('load', function () {
  //   $('.loader').fadeOut();
  // });


  function runMasonry(ltr) {
    $('.view-display-id-all_newsletter .view-content').masonry({
      originLeft: ltr,
      itemSelector: '.views-row',
      columnWidth: '.views-row',
      percentPosition: true
    });
  }

  $(window).bind("load", function () {
    // Masonry for Newsletter.
    var ltr = true;
    if ($('html').attr('dir') == 'rtl') {
      ltr = false;
    }
    runMasonry(ltr);
    var masonryUpdate = function () {
      $('.view-display-id-all_newsletter .view-content').imagesLoaded(function () {
        runMasonry(ltr);
      })
    }
    $(document).on('click', masonryUpdate);
    $(document).ajaxComplete(masonryUpdate);
  });

})(window.jQuery, window._, window.Drupal, window.drupalSettings);

/**
 * @file
 */

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del');

gulp.task('sass', function () {
    gulp.src(['./assets/scss/seeds-toolbar.scss','./assets/scss/rtl/seeds-toolbar-rtl.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('build', function () {
    gulp.src(['./assets/scss/seeds-toolbar.scss', './assets/scss/rtl/seeds-toolbar-rtl.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('watch', ['sass'], function () {
    gulp.watch('./assets/scss/**/*.scss', ['sass']);
});

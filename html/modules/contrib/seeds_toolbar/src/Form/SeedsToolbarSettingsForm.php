<?php

namespace Drupal\seeds_toolbar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the seeds_toolbar settings form.
 */
class SeedsToolbarSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'seeds_toolbar.settings',
    ];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'seeds_toolbar_settings';
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('seeds_toolbar.settings');
    $form['style'] = [
      '#type' => 'select',
      '#title' => t('Style'),
      '#options' => [
        'light' => 'Light',
        'dark' => 'Dark',
      ],
      '#default_value' => $config->get('style'),
    ];

    $form['compact'] = [
      '#type' => 'checkbox',
      '#title' => t('Compact'),
      '#default_value' => $config->get('compact'),
      '#description' => t('Choose if you don\'t want the toolbar to be opened by default.'),
    ];

    $form['support'] = [
      '#type' => 'textfield',
      '#title' => t('Support URL'),
      '#default_value' => $config->get('support'),
      '#description' => t('If you want to add your support system URL to be shown on client area. Leave empty to hide the link.'),
    ];

    $form['custom_style'] = [
      '#type' => 'textfield',
      '#title' => t('Custom Style'),
      '#default_value' => $config->get('custom_style'),
      '#description' => t('Provide your link to css file for use in seeds toolbar.'),
    ];

    $form['dark_logo'] = [
      '#type' => 'textfield',
      '#title' => t('Custom Dark Logo Link'),
      '#default_value' => $config->get('dark_logo'),
      '#description' => t('Leave empty to use the default logo.'),
    ];

    $form['light_logo'] = [
      '#type' => 'textfield',
      '#title' => t('Custom Light Logo Link'),
      '#default_value' => $config->get('light_logo'),
      '#description' => t('Leave empty to use the default logo.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface:validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface:submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('seeds_toolbar.settings')
      ->set('compact', $form_state->getValue('compact'))
      ->set('support', $form_state->getValue('support'))
      ->set('custom_style', $form_state->getValue('custom_style'))
      ->set('style', $form_state->getValue('style'))
      ->set('light_logo', $form_state->getValue('light_logo'))
      ->set('dark_logo', $form_state->getValue('dark_logo'))
      ->save();
  }

}

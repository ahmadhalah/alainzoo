(function ($, _, Drupal, drupalSettings) {
  'use strict';

  function generateSlickOptions(rows, responsiveRows) {
    var slickOptions = {
      infinite: true,
      lazyLoad: 'progressive',
      rows: rows,
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
      arrows: false,
      speed: 300,
      swipeToSlide: false,
      mobileFirst: false,
      asNavFor: '',
      accessibility: true,
      adaptiveHeight: false,
      autoplay: true,
      autoplaySpeed: 5000,
      pauseOnHover: true,
      pauseOnDotsHover: false,
      downArrow: false,
      downArrowTarget: '',
      downArrowOffset: 0,
      centerMode: false,
      centerPadding: '50px',
      dotsClass: 'slick-dots',
      draggable: true,
      fade: false,
      focusOnSelect: false,
      rtl: ($('html').attr('lang') === 'ar') ? true : false,
      swipe: true,
      edgeFriction: 0.35,
      touchMove: true,
      touchThreshold: 5,
      useCSS: true,
      cssEase: 'ease',
      cssEaseBezier: '',
      cssEaseOverride: '',
      useTransform: true,
      easing: 'linear',
      variableWidth: false,
      vertical: false,
      verticalSwiping: false,
      waitForAnimate: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            rows: responsiveRows
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            rows: responsiveRows
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            rows: responsiveRows,
            dots: false
          }
        }
      ]
    };
    return slickOptions;
  }

  function addValues(values, item) {
    var checked = false;
    if (values.length) {
      for (var i = 0; i < values.length; i++) {
        if (!checked) {
          if (values[i].id === item.id) {
            values[i].value = item.value;
            checked = true;
            continue;
          }
        }
      }
      if (!checked) {
        values.push(item);
        checked = true;
      }
    } else {
      values.push(item);
    }

    return values;
  }

  function generateEventsMessage() {
    if ($('#edit-field-what-s-new div.form-item:not(.custom-plan-hidden)').length === 0) {
      $('.empty-events').show();
    } else {
      $('.empty-events').hide();
    }
  }

  function generateSuggestedTicketsTable(tickets) {

    var data = [];
    var SuggestedTickets = drupalSettings.ain_visit.suggested_tickets;
    if (SuggestedTickets.length) {
      $(tickets).each(function () {
        var ticketId = $(this)[0];
        $(SuggestedTickets).each(function () {
          var suggestedTicketId = $(this)[0].nid;
          if (ticketId == suggestedTicketId) {
            data.push($(this)[0]);
          }
        });
      });

      if (data.length) {
        var message = '<table class="table">';
        $(data).each(function () {

          message += '<tr><td>' + Drupal.t('Suggested ticket for Adult') + '</td><td>' + $(this)[0].title + '</td><td>' + $(this)[0].adult + ' ' + Drupal.t('AED') + '</td></tr>';
          message += '<tr><td>' + Drupal.t('Suggested ticket for Child') + '</td><td>' + $(this)[0].title + '</td><td>' + $(this)[0].child + ' ' + Drupal.t('AED') + '</td></tr>';
        });
        message += '</table>';
        $('.suggested-tickets-wrapper').html(message);
        $("#edit-body-0-value").val(message);
        if (typeof (CKEDITOR) != "undefined") {
          CKEDITOR.instances['edit-body-0-value'].setData(message);
        }
      } else {
        $('.suggested-tickets-wrapper').html('');
        $("#edit-body-0-value").val('');
        $("#edit-field-suggested-ticket-en-0-value").val(0);
        $("#edit-field-suggested-ticket-ar-0-value").val(0);
        if (typeof (CKEDITOR) != "undefined") {
          CKEDITOR.instances['edit-body-0-value'].setData('');
        }
      }
    } else {
      $('.suggested-tickets-wrapper').html('');
      $("#edit-body-0-value").val('');
      $("#edit-field-suggested-ticket-en-0-value").val(0);
      $("#edit-field-suggested-ticket-ar-0-value").val(0);
      if (typeof (CKEDITOR) != "undefined") {
        CKEDITOR.instances['edit-body-0-value'].setData('');
      }
    }
  }

  function generateSuggestedTickets() {
    // @todo: replace hardcoded ids.
    if ($('#customize-your-plan-wrapper .suggested-tickets-wrapper').length === 0) {
      $('#customize-your-plan-wrapper').append('<div class="suggested-tickets-wrapper"></div>');
    }

    var contentIds = drupalSettings.ain_visit.content_ids;

    var attractions = [];
    var animals = [];

    var attractionsMap = [];
    attractionsMap['szdlc'] = parseInt(contentIds.ain_misc_attraction_szdlc);
    attractionsMap['safari'] = parseInt(contentIds.ain_misc_attraction_safari);

    var tickets = [];

    $('.main-container form#node-visit-plan-form input[name^="field_attraction"]').each(function () {
      if ($(this).prop('checked')) {
        attractions.push(parseInt($(this).attr('value')));
      }
    });

    $('.main-container form#node-visit-plan-form input[name^="field_animal"]').each(function () {
      if ($(this).prop('checked')) {
        animals.push(parseInt($(this).attr('value')));
      }
    });

    if (animals.length) {
      if (attractions.length) {
        if ($.inArray(attractionsMap['szdlc'], attractions) !== -1 && $.inArray(attractionsMap['safari'], attractions) !== -1) {
          // Animals and SZDLC and Safari - Safari Explorer.
          tickets.push(parseInt(contentIds.ain_misc_ticket_safari_explorer_en), parseInt(contentIds.ain_misc_ticket_safari_explorer_ar));
          $("#edit-field-suggested-ticket-en-0-value").val(parseInt(contentIds.ain_misc_ticket_safari_explorer_en));
          $("#edit-field-suggested-ticket-ar-0-value").val(parseInt(contentIds.ain_misc_ticket_safari_explorer_ar));
          generateSuggestedTicketsTable(tickets);
        } else {
          if ($.inArray(attractionsMap['szdlc'], attractions) !== -1) {
            // Animals and SZDLC - Explorer Ticket.
            tickets.push(parseInt(contentIds.ain_misc_ticket_explorer_ticket_en), parseInt(contentIds.ain_misc_ticket_explorer_ticket_ar));
            $("#edit-field-suggested-ticket-en-0-value").val(parseInt(contentIds.ain_misc_ticket_explorer_ticket_en));
            $("#edit-field-suggested-ticket-ar-0-value").val(parseInt(contentIds.ain_misc_ticket_explorer_ticket_ar));
            generateSuggestedTicketsTable(tickets);
          }
          if ($.inArray(attractionsMap['safari'], attractions) !== -1) {
            // Animals and Safari - Safari Entertainer.
            tickets.push(parseInt(contentIds.ain_misc_ticket_safari_entertainer_en), parseInt(contentIds.ain_misc_ticket_safari_entertainer_ar));
            $("#edit-field-suggested-ticket-en-0-value").val(parseInt(contentIds.ain_misc_ticket_safari_entertainer_en));
            $("#edit-field-suggested-ticket-ar-0-value").val(parseInt(contentIds.ain_misc_ticket_safari_entertainer_ar));
            generateSuggestedTicketsTable(tickets);
          }
        }
      } else {
        // Animals Only - Entertainer Ticket.
        tickets.push(parseInt(contentIds.ain_misc_ticket_entertainer_ticket_en), parseInt(contentIds.ain_misc_ticket_entertainer_ticket_ar));
        $("#edit-field-suggested-ticket-en-0-value").val(parseInt(contentIds.ain_misc_ticket_entertainer_ticket_en));
        $("#edit-field-suggested-ticket-ar-0-value").val(parseInt(contentIds.ain_misc_ticket_entertainer_ticket_ar));
        generateSuggestedTicketsTable(tickets);
      }
    } else {
      $('.suggested-tickets-wrapper').html('');
      $("#edit-body-0-value").val('');
      $("#edit-field-suggested-ticket-en-0-value").val(0);
      $("#edit-field-suggested-ticket-ar-0-value").val(0);
      if (typeof (CKEDITOR) != "undefined") {
        CKEDITOR.instances['edit-body-0-value'].setData('');
      }
    }
  }

  function generateVisitDuration() {
    if ($('#customize-your-plan-wrapper .visit-duration-wrapper').length === 0) {
      $('#customize-your-plan-wrapper').append('<div class="visit-duration-wrapper"></div>');
    }

    var totalVisitDuration = 0;

    $('.main-container form#node-visit-plan-form input[name^="field_attraction"]').each(function () {
      if ($(this).prop('checked')) {
        totalVisitDuration += parseInt($(this).parent().find('.item-duration').text().trim()) || 0;
      }
    });

    $('.main-container form#node-visit-plan-form input[name^="field_animal"]').each(function () {
      if ($(this).prop('checked')) {
        totalVisitDuration += parseInt($(this).parent().find('.item-duration').text().trim()) || 0;
      }
    });
    $('.main-container form#node-visit-plan-form input[name^="field_experience_reference"]').each(function () {
      if ($(this).prop('checked')) {
        totalVisitDuration += parseInt($(this).parent().find('.item-duration').text().trim()) || 0;
      }
    });

    $('.main-container form#node-visit-plan-form input[name^="field_what_s_new"]').each(function () {
      if ($(this).prop('checked')) {
        totalVisitDuration += parseInt($(this).parent().find('.item-duration').text().trim()) || 0;
      }
    });

    $('#edit-field-duration-wrapper input').val(totalVisitDuration);

    var message = '<table class="table">';
    message += '<tr><td>' + Drupal.t('Estimated Visit Duration') + ': ' + totalVisitDuration + ' ' + Drupal.t('Minutes') + '</td></tr>';
    message += '</table>';
    $(".visit-duration-wrapper").html(message);
  }

  function currentDay() {
    var d = new Date();
    var weekday = [Drupal.t("Sunday"), Drupal.t("Monday"), Drupal.t("Tuesday"), Drupal.t("Wednesday"), Drupal.t("Thursday"), Drupal.t("Friday"), Drupal.t("Saturday")];

    return weekday[d.getDay()];
  }

  Drupal.behaviors.planVisit = {
    attach: function (context, settings) {
      $(window).bind('load', function () {

        $('#edit-body-wrapper, #edit-field-suggested-ticket-en-wrapper, #edit-field-suggested-ticket-ar-wrapper').addClass('custom-plan-hidden');
        $('#edit-field-duration-wrapper').addClass('custom-plan-hidden');

        var emptyMessage = Drupal.t('No events matching your visit date.');
        if ($('#edit-field-what-s-new .empty-events').length === 0) {
          $('#edit-field-what-s-new').prepend('<div class="empty-events">' + emptyMessage + '</div>');
        }

        $('.main-container form#node-visit-plan-form input:radio[name="field_plan"]').change(function () {
          $('#edit-title-wrapper input').val('');
          $('#edit-field-plan .form-item-field-plan label').removeClass('plan-selected');
          $(this).parents('.form-item-field-plan label').addClass('plan-selected');
          if ($(this).val() === '_none') {
            $('#customize-your-plan-wrapper').removeClass('custom-plan-hidden');
            $('html, body').animate({
              scrollTop: $('#customize-your-plan-wrapper').offset().top
            }, 500);
          } else {
            var recommendedPlanTitle = $(this).find('~ .views-field-title .field-content').text().trim();
            $('#edit-title-wrapper input').val(recommendedPlanTitle + ' - ' + currentDay());
            $('#customize-your-plan-wrapper').addClass('custom-plan-hidden');
          }
        });

        $('.main-container form#node-visit-plan-form input[name^="field_experience_reference"]').once().change(function () {
          if ($(this).prop('checked')) {
            $(this).parent('label').addClass('experience-selected');
          } else {
            $(this).parent('label').removeClass('experience-selected');
          }
          generateVisitDuration();
        });

        $('.main-container form#node-visit-plan-form input[name^="field_attraction"]').once().change(function () {
          if ($(this).prop('checked')) {
            $(this).parent('label').addClass('attraction-selected');
          } else {
            $(this).parent('label').removeClass('attraction-selected');
          }
          generateVisitDuration();
          generateSuggestedTickets();
        });

        $('.main-container form#node-visit-plan-form input[name^="field_animal"]').once().change(function () {
          generateVisitDuration();
          generateSuggestedTickets();
        });

        $('.main-container form#node-visit-plan-form input[name^="field_what_s_new"]').once().change(function () {
          generateVisitDuration();
        });

        var defaultFilter = 'none';
        if ($('.main-container form#node-visit-plan-form #edit-field-animal > .form-item span.tablinks').length && !$('.main-container form#node-visit-plan-form #edit-field-animal').parent().find('.animals-filters-container').length) {
          $('#edit-field-animal > .form-item').each(function () {
            if (!$('#edit-field-animal').parent().find('.animals-filters-container').length) {
              $('#edit-field-animal').parent().prepend('<div class="animals-filters-container"></div>');
            }
            var spanElement = $(this).find('span.tablinks');
            var spanAttribute = 'animal-';
            var cls = spanElement.attr('class').split(' ');
            for (var i = 0; i < cls.length; i++) {
              if (cls[i].indexOf(spanAttribute) > -1) {
                var attributeName = spanAttribute + cls[i].slice(spanAttribute.length, cls[i].length);
                if (defaultFilter == 'none') {
                  defaultFilter = attributeName;
                }
                spanElement.attr('data-filter', '.' + attributeName);
                spanElement.attr('data-type', attributeName);
                if (!$('#edit-field-animal').find('.animals-slider.' + attributeName).length) {
                  $('#edit-field-animal').prepend('<div class="slider-hidden animals-slider ' + attributeName + '"></div>');
                }
                $(this).appendTo($('#edit-field-animal').find('.animals-slider.' + attributeName));
                if (!($('#edit-field-animal').parent().find('.animals-filters-container').find($('span.' + attributeName)).length)) {
                  spanElement.prependTo($('#edit-field-animal').parent().find('.animals-filters-container'));
                } else {
                  spanElement.remove();
                }
              }
            }
          });
        }

        if (!$('.main-container form#node-visit-plan-form #edit-field-animal').parent().find('.animals-filters-container').hasClass('filter-started')) {
          $('#edit-field-animal').parent().find('.animals-filters-container').addClass('filter-started');
          $('#edit-field-animal .animals-slider').each(function () {
            var slickOptions = generateSlickOptions(2, 2);
            $(this).slick(slickOptions);
          });
          $('.animals-filters-container span').each(function () {
            var dataSelector = $(this).attr('data-type');
            $(this).click(function (event) {
              $('.animals-filters-container span').removeClass('filter-active');
              $(this).addClass('filter-active');
              $('div.animals-slider').addClass('slider-hidden');
              $('div.' + dataSelector).removeClass('slider-hidden');
            });
          });
          $('.animals-filters-container span:first-of-type').addClass('filter-active');
          var dataType = $('.animals-filters-container span:first-of-type').attr('data-type');
          $('div.' + dataType).removeClass('slider-hidden');
        }

        if ($('.field--name-field-visit-date > .help-block')) {
          var helpBlock = $('.field--name-field-visit-date > .help-block');
          helpBlock
            .addClass('pull-right')
            .prependTo($('.field--name-field-visit-date > label'));
        }

        $('.main-container form#node-visit-plan-form input.form-date[name^="field_visit_date"]').change(function () {
          $('input[name^="field_what_s_new"]:checked').prop('checked', false);
          var visitDate = $(this).val();
          if (visitDate) {
            $('input[name^="field_what_s_new"]').each(function () {
              $(this).parent().parent().addClass('custom-plan-hidden');
              var recommendedDate = $(this).parent().find('.event-date').text().trim().split(',');
              var showEvent = false;
              for (var i = 0; i < recommendedDate.length; i++) {
                var startDate = recommendedDate[i].trim().split('/')[0].trim();
                var endDate = recommendedDate[i].trim().split('/')[1].trim();
                if (visitDate >= startDate && visitDate <= endDate) {
                  showEvent = true;
                }
              }
              if (showEvent) {
                $(this).parent().parent().removeClass('custom-plan-hidden');
              } else {
                $(this).prop('checked', false);
                $(this).parent().parent().addClass('custom-plan-hidden');
              }
            });
          } else {
            $('input[name^="field_what_s_new"]').parent().parent().removeClass('custom-plan-hidden');
          }

          generateEventsMessage();

          if ($('#edit-field-what-s-new').length && $('#edit-field-what-s-new .form-item').length) {
            var slickOptions = generateSlickOptions(1, 1);
            slickOptions.slide = '#edit-field-what-s-new div.form-item:not(.custom-plan-hidden)';
            if ($('#edit-field-what-s-new').hasClass('slick-initialized')) {
              $('#edit-field-what-s-new').slick('unslick');
            }
            $('#edit-field-what-s-new').slick(slickOptions);
          }

        });
      });

      $(window).on("resize", function () {
        var customizePlan = $('.main-container form#node-visit-plan-form #edit-field-plan .form-item label[for="edit-field-plan-none"]');
        var planHeight = customizePlan.outerWidth() * .839;
        customizePlan.innerHeight(planHeight);
      }).resize();

    }
  };

  Drupal.behaviors.planVisitLocalStorage = {
    attach: function (context, settings) {
      $(window).bind('load', function () {

        if ($('.add-to-plan').length) {
          var planVisitStorage = localStorage.getItem('Drupal.planVisit.item');
          planVisitValues = JSON.parse(planVisitStorage);

          var itemCheck = false;
          $.each(planVisitValues, function (key, element) {
            if (element.id === $('.add-to-plan').attr('data-id') && element.value === true) {
              itemCheck = true;
            }
          });
          if (itemCheck) {
            $('.add-to-plan').parent().addClass('make-hidden');
            $('.remove-from-plan').parent().removeClass('make-hidden');
          } else {
            $('.add-to-plan').parent().removeClass('make-hidden');
            $('.remove-from-plan').parent().addClass('make-hidden');
          }
        }

        $('.add-to-plan').click(function (event) {
          if ($(this).parents('.container').find('.alert-danger').length) {
            $(this).parents('.container').find('.alert-danger').remove();
          }
          var isStorage = (typeof (window['localStorage']) != undefined);
          var isJSON = (typeof (window['JSON']) != undefined);

          if (!isStorage || !isJSON) {
            return;
          }

          var planVisitStorage = localStorage.getItem('Drupal.planVisit.item');
          if (planVisitStorage === null) {
            localStorage.setItem('Drupal.planVisit.item', JSON.stringify(new Array()));
            planVisitStorage = localStorage.getItem('Drupal.planVisit.item');
          }

          planVisitValues = JSON.parse(planVisitStorage);
          planVisitValues = addValues(planVisitValues, {'id': $(this).attr('data-id'), 'type': 'checkbox', 'value': true});
          localStorage.setItem('Drupal.planVisit.item', JSON.stringify(planVisitValues));
          if (!$(this).parents('.container').find('.alert-success').length) {
            $(this).parents('.container').append('<div class="alert alert-success alert-dismissible" role="alert">\
                <strong>' + $(this).attr('data-title') + ':</strong> ' + Drupal.t('was added successfully to your plan.') + '\
                <button type="button" class="close" data-dismiss="alert" aria-label="' + Drupal.t('Close') + '">\
                  <span aria-hidden="true">&times;</span>\
                </button>\
              </div>');
          }

          $('.add-to-plan').parent().addClass('make-hidden');
          $('.remove-from-plan').parent().removeClass('make-hidden');

        });

        $('.remove-from-plan').click(function (event) {
          if ($(this).parents('.container').find('.alert-success').length) {
            $(this).parents('.container').find('.alert-success').remove();
          }

          var isStorage = (typeof (window['localStorage']) != undefined);
          var isJSON = (typeof (window['JSON']) != undefined);

          if (!isStorage || !isJSON) {
            return;
          }

          var planVisitStorage = localStorage.getItem('Drupal.planVisit.item');
          if (planVisitStorage === null) {
            localStorage.setItem('Drupal.planVisit.item', JSON.stringify(new Array()));
            planVisitStorage = localStorage.getItem('Drupal.planVisit.item');
          }

          planVisitValues = JSON.parse(planVisitStorage);
          planVisitValues = addValues(planVisitValues, {'id': $(this).attr('data-id'), 'type': 'checkbox', 'value': false});
          localStorage.setItem('Drupal.planVisit.item', JSON.stringify(planVisitValues));
          if (!$(this).parents('.container').find('.alert-danger').length) {
            $(this).parents('.container').append('<div class="alert alert-danger alert-dismissible" role="alert">\
                <strong>' + $(this).attr('data-title') + ':</strong> ' + Drupal.t('was removed successfully from your plan.') + '\
                <button type="button" class="close" data-dismiss="alert" aria-label="' + Drupal.t('Close') + '">\
                  <span aria-hidden="true">&times;</span>\
                </button>\
              </div>');
          }

          $('.add-to-plan').parent().removeClass('make-hidden');
          $('.remove-from-plan').parent().addClass('make-hidden');

        });

        if ($('.main-container form#node-visit-plan-form').length) {

          var isStorage = (typeof (window['localStorage']) != undefined);
          var isJSON = (typeof (window['JSON']) != undefined);

          if (!isStorage || !isJSON) {
            return;
          }

          var planVisitStorage = localStorage.getItem('Drupal.planVisit.item');

          var planVisitValues = [];
          var planVisitFormInputs = {
            'date': [
              'edit-field-visit-date-wrapper'
            ],
            'radio': [
              'edit-field-plan-wrapper'
            ],
            'text': [
              'edit-title-wrapper'
            ],
            'checkbox': [
              'edit-field-attraction',
              'edit-field-animal',
              'edit-field-experience-reference',
              'edit-field-what-s-new'
            ]
          };

          if (planVisitStorage !== null) {
            planVisitValues = JSON.parse(planVisitStorage);
            $.each(planVisitValues, function (key, element) {
              switch (element.type) {
                case 'date':
                case 'text':
                  var inputElement = $('#' + element.id);
                  inputElement.val(element.value);
                  break;

                case 'radio':
                  var inputElement = $('#' + element.id).find('input[value="' + element.value + '"]');
                  inputElement.prop('checked', element.value);
                  break;

                case 'checkbox':
                  var inputElement = $('#' + element.id);
                  inputElement.prop('checked', element.value);
                  break;
              }
            });
          }

          $.each(planVisitFormInputs, function (key, value) {
            switch (key) {
              case 'date':
              case 'text':
                $.each(value, function (k, v) {
                  var inputField = $('#' + v).find('input');
                  inputField.change(function () {
                    planVisitValues = addValues(planVisitValues, {'id': $(this).attr('id'), 'type': key, 'value': $(this).val()});
                    localStorage.setItem('Drupal.planVisit.item', JSON.stringify(planVisitValues));
                  });
                });
                break;

              case 'radio':
                $.each(value, function (k, v) {
                  var inputField = $('#' + v).find('input');
                  inputField.change(function () {
                    planVisitValues = addValues(planVisitValues, {'id': v, 'type': key, 'value': $(this).val()});
                    localStorage.setItem('Drupal.planVisit.item', JSON.stringify(planVisitValues));
                  });
                });
                break;

              case 'checkbox':
                $.each(value, function (k, v) {
                  var inputField = $('#' + v).find('input');
                  inputField.each(function () {
                    $(this).change(function () {
                      planVisitValues = addValues(planVisitValues, {'id': $(this).attr('id'), 'type': key, 'value': $(this).prop('checked')});
                      localStorage.setItem('Drupal.planVisit.item', JSON.stringify(planVisitValues));
                    });
                  });
                });
                break;
            }
          });

          $('.main-container form#node-visit-plan-form #edit-field-plan .form-item-field-plan label').removeClass('plan-selected');
          $('.main-container form#node-visit-plan-form input[name="field_plan"]:checked').parents('.form-item-field-plan label').addClass('plan-selected');
          if ($('.main-container form#node-visit-plan-form input[name="field_plan"]:checked').val() === '_none') {
            $('.main-container form#node-visit-plan-form #customize-your-plan-wrapper').removeClass('custom-plan-hidden');
          } else {
            $('.main-container form#node-visit-plan-form #customize-your-plan-wrapper').addClass('custom-plan-hidden');
            $('#edit-title-wrapper input').val('');
            $('#edit-field-plan .form-item-field-plan label').removeClass('plan-selected');
            $('.main-container form#node-visit-plan-form input[name="field_plan"]:checked').parents('.form-item-field-plan label').addClass('plan-selected');
            var recommendedPlanTitle = $('.main-container form#node-visit-plan-form input[name="field_plan"]:checked').find('~ .views-field-title .field-content').text().trim();
            $('#edit-title-wrapper input').val(recommendedPlanTitle + ' - ' + currentDay());
          }

          $('.main-container form#node-visit-plan-form input[name^="field_experience_reference"]').each(function () {
            $(this).parent('label').removeClass('experience-selected');
            if ($(this).prop('checked')) {
              $(this).parent('label').addClass('experience-selected');
            }
          });

          $('.main-container form#node-visit-plan-form input[name^="field_attraction"]').each(function () {
            $(this).parent('label').removeClass('attraction-selected');
            if ($(this).prop('checked')) {
              $(this).parent('label').addClass('attraction-selected');
            }
          });

          var visitDate = $('.main-container form#node-visit-plan-form input.form-date[name^="field_visit_date"]').val();
          if (visitDate) {
            $('input[name^="field_what_s_new"]').each(function () {
              var recommendedDate = $(this).parent().find('.event-date').text().trim().split(',');
              var showEvent = false;
              for (var i = 0; i < recommendedDate.length; i++) {
                var startDate = recommendedDate[i].trim().split('/')[0].trim();
                var endDate = recommendedDate[i].trim().split('/')[1].trim();
                if (visitDate >= startDate && visitDate <= endDate) {
                  showEvent = true;
                }
              }
              if (showEvent) {
                $(this).parent().parent().removeClass('custom-plan-hidden');
              } else {
                $(this).prop('checked', false);
                $(this).parent().parent().addClass('custom-plan-hidden');
              }
            });
          }

          generateEventsMessage();
          generateVisitDuration();
          generateSuggestedTickets();

          if ($('.main-container form#node-visit-plan-form #edit-field-what-s-new').length && $('.main-container form#node-visit-plan-form #edit-field-what-s-new .form-item').length) {
            var slickOptions = generateSlickOptions(1, 1);
            slickOptions.slide = '.main-container form#node-visit-plan-form #edit-field-what-s-new div.form-item:not(.custom-plan-hidden)';
            if ($('.main-container form#node-visit-plan-form #edit-field-what-s-new').hasClass('slick-initialized')) {
              $('.main-container form#node-visit-plan-form #edit-field-what-s-new').slick('unslick');
            }
            $('.main-container form#node-visit-plan-form #edit-field-what-s-new').slick(slickOptions);
          }

        }

      });
    }
  };
}
)(window.jQuery, window._, window.Drupal, window.drupalSettings, window.localStorage, window.CKEDITOR);

<?php

/**
 * @file
 * Displays plan your visit form as a block
 */

namespace Drupal\ain_visit\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Views;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "plan_your_visit_form_block",
 *   admin_label = @Translation("Plan your visit form"),
 *   category = @Translation("Forms")
 * )
 */
class VisitFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface.
   */
  protected $entityFormBuilder;

  /**
   * Constructs a new NodeFormBlock plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, EntityFormBuilderInterface $entityFormBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);

    $this->entityTypeManager = $entityTypeManager;
    $this->entityFormBuilder = $entityFormBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('entity.form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $node_type = 'visit_plan';

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => $node_type,
    ]);

    $config_factory = \Drupal::configFactory();
    $config = $config_factory->get('ain_misc.settings');
    $do_dont_nid = ($config->get('ain_misc_do_do_not')) ? $config->get('ain_misc_do_do_not') : 0;

    $path = 'node/' . $do_dont_nid;
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $img = '/themes/ain/images/do-dont-' . $language . '.png';
    $output = '<a class="use-ajax visit-plan-modal" data-dialog-type="modal" href="' . $path . '"><img src="' . $img . '" /></a>';
    $build['form'] = $this->entityFormBuilder->getForm($node);

    // alter the form only if displayed from this block leaving the admin node forms intact.
    $build['form']['#attached']['library'][] = 'ain_map/ain_visit_map';
    $build['form']['field_plan_image']['#access'] = FALSE;
    $build['form']['field_plan_details']['#access'] = FALSE;
    $build['form']['field_is_recommended']['#access'] = FALSE;
    $build['form']['field_items']['#access'] = FALSE;
    $build['form']['advanced']['#access'] = FALSE;
    $build['form']['field_latitude']['#access'] = false;
    $build['form']['field_text']['#access'] = false;
    $build['form']['field_number']['#access'] = false;
    $build['form']['field_minor']['#access'] = false;
    $build['form']['status']['widget']['value']['#default_value'] = FALSE;
    $build['form']['title']['widget'][0]['value']['#title_display'] = 'before';
    $build['form']['status']['widget']['value']['#value'] = FALSE;
    $build['form']['status']['widget']['value']['#checked'] = FALSE;
    $build['form']['status']['widget']['value']['#access'] = FALSE;

    $build['form']['field_visit_date']['widget'][0]['value']['#description'] = $output;

    // Attach drupalSettings for suggested_tickets only to plan_visit frontend form.
    $language_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $args = [$language_code];
    $view = Views::getView('api_services');
    $view->setArguments($args);
    $view->setDisplay('suggested_tickets');
    $view->preExecute();
    $view->execute();
    $content = $view->render();
    $data_string = $content['#markup']->jsonSerialize();
    $data = Json::decode($data_string);

    // Load configurations.
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->get('ain_misc.settings');

    $ids_data = [
      'ain_misc_attraction_szdlc' => ($config->get('ain_misc_attraction_szdlc')) ? $config->get('ain_misc_attraction_szdlc') : 0,
      'ain_misc_attraction_safari' => ($config->get('ain_misc_attraction_safari')) ? $config->get('ain_misc_attraction_safari') : 0,
      'ain_misc_ticket_safari_explorer_en' => ($config->get('ain_misc_ticket_safari_explorer_en')) ? $config->get('ain_misc_ticket_safari_explorer_en') : 0,
      'ain_misc_ticket_safari_explorer_ar' => ($config->get('ain_misc_ticket_safari_explorer_ar')) ? $config->get('ain_misc_ticket_safari_explorer_ar') : 0,
      'ain_misc_ticket_explorer_ticket_en' => ($config->get('ain_misc_ticket_explorer_ticket_en')) ? $config->get('ain_misc_ticket_explorer_ticket_en') : 0,
      'ain_misc_ticket_explorer_ticket_ar' => ($config->get('ain_misc_ticket_explorer_ticket_ar')) ? $config->get('ain_misc_ticket_explorer_ticket_ar') : 0,
      'ain_misc_ticket_safari_entertainer_en' => ($config->get('ain_misc_ticket_safari_entertainer_en')) ? $config->get('ain_misc_ticket_safari_entertainer_en') : 0,
      'ain_misc_ticket_safari_entertainer_ar' => ($config->get('ain_misc_ticket_safari_entertainer_ar')) ? $config->get('ain_misc_ticket_safari_entertainer_ar') : 0,
      'ain_misc_ticket_entertainer_ticket_en' => ($config->get('ain_misc_ticket_entertainer_ticket_en')) ? $config->get('ain_misc_ticket_entertainer_ticket_en') : 0,
      'ain_misc_ticket_entertainer_ticket_ar' => ($config->get('ain_misc_ticket_entertainer_ticket_ar')) ? $config->get('ain_misc_ticket_entertainer_ticket_ar') : 0,
    ];
    $build['form']['#attached']['drupalSettings']['ain_visit']['suggested_tickets'] = $data;
    $build['form']['#attached']['drupalSettings']['ain_visit']['content_ids'] = $ids_data;

    return $build;
  }

}

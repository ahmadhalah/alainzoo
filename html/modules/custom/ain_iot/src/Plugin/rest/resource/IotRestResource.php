<?php

namespace Drupal\ain_iot\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "iot_rest_resource",
 *   label = @Translation("Iot rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/iot",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/iot"
 *   }
 * )
 */
class IotRestResource extends ResourceBase {
  use AinResponseResourceTrait;
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new IotRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ain_iot'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post(array $data) {
    $kit = isset($data['kit']) ? $data['kit'] : '';
    $tmprtr = isset($data['tmprtr']) ? $data['tmprtr'] : '';
    $hmdt = isset($data['hmdt']) ? $data['hmdt'] : '';
    $dst = isset($data['dst']) ? $data['dst'] : '';

    if(!is_numeric($kit) || !is_numeric($tmprtr) || !is_numeric($hmdt) || !is_numeric($dst)) {
      $message = 'Error with data.';
      return $this->ain_response('Failed', $message, 422); 
    }

    $record = entity_create('iot_record', [
      'kit' => $kit,
      'tmprtr' => $tmprtr,
      'hmdt' => $hmdt,
      'dst' => $dst,
    ]);

    $record->save();
    $message = 'Record added successfully';
    return $this->ain_response('Success', $message, 200); 
  }
  public function get(){
    $now = strtotime('now');
    $before_two_hours = strtotime('now -2 hours');

    $query = db_query("select avg(dst) as dst, avg(tmprtr) as tmprtr, avg(hmdt) as hmdt from iot_record where created between $before_two_hours and $now");
    $result= $query->fetchAll();

    return new JsonResponse($result, 200);
  }
}

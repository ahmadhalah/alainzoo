<?php

namespace Drupal\ain_iot\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for IOT Records entities.
 */
class IotRecordViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}

<?php

namespace Drupal\ain_iot\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the IOT Records entity.
 *
 * @ingroup ain_iot
 *
 * @ContentEntityType(
 *   id = "iot_record",
 *   label = @Translation("IOT Records"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ain_iot\IotRecordListBuilder",
 *     "views_data" = "Drupal\ain_iot\Entity\IotRecordViewsData",
 *
 *   },
 *   base_table = "iot_record",
 *   admin_permission = "administer iot records entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class IotRecord extends ContentEntityBase implements IotRecordInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function getKit() {
    return $this->get('kit')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKit($name) {
    $this->set('kit', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTmprtr()
  {
    return $this->get('tmprtr')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTmprtr($name)
  {
    $this->set('tmprtr', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHmdt()
  {
    return $this->get('hmdt')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHmdt($name)
  {
    $this->set('hmdt', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDst()
  {
    return $this->get('dst')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDst($name)
  {
    $this->set('dst', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['kit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Kit ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', false)
      ->setDisplayConfigurable('view', false);

    $fields['tmprtr'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Temperature'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', false)
      ->setDisplayConfigurable('view', false);

    $fields['hmdt'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('hmdt'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', false)
      ->setDisplayConfigurable('view', false);

    $fields['dst'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Dst'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', false)
      ->setDisplayConfigurable('view', false);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    return $fields;
  }

}

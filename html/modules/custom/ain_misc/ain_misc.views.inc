<?php

/**
 * Implements hook_views_data_alter().
 */
function ain_misc_views_data_alter(array &$data) {
  $data['users']['user_custom_email'] = array(
    'title' => t('Ain email address'),
    'field' => array(
      'title' => t('Ain email address'),
      'help' => t('User email address.'),
      'id' => 'user_custom_email',
    ),
  );
}

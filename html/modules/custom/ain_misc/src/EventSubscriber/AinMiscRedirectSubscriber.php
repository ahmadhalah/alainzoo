<?php

/**
 * @file
 * Contains \Drupal\ain_misc\EventSubscriber\AinMiscRedirectSubscriber
 */

namespace Drupal\ain_misc\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\user\Entity\User;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class AinMiscRedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This announces which events you want to subscribe to.
    // We only need the request event.
    return([
      KernelEvents::REQUEST => [
        ['redirectUserOnDemand'],
      ]
    ]);
  }

  /**
   * Redirect requests for plan your visit page to most recent upcoming plan.
   *
   * @param GetResponseEvent $event
   * @return void
   */
  public function redirectUserOnDemand(GetResponseEvent $event) {
    $request = $event->getRequest();

    // Get the user object.
    $user = User::load(\Drupal::currentUser()->id());
    // Get the user id.
    $uid = $user->uid->value;

    if ($uid && $uid != 1) {
      $username = $user->getAccountName();
      if (!is_numeric($username)) {
        // get the user edit form url.
        $edit_page = $user->url('edit-form');
        // get the lanuage code:
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $edit_page = '/user/' . $uid . '/edit';
        if ($language == 'ar') {
          $edit_page = '/ar' . $edit_page;
        }
        // get current url.
        $current_url = \Drupal::request()->getRequestUri();

        // prevent redirect loop.
        if (!AinMiscRedirectSubscriber::testUrl($current_url) && $current_url != $edit_page) {
          $message = 'Please enter your mobile number.';
          drupal_set_message(new TranslatableMarkup($message), 'error', FALSE);
          // redirect to user edit form if there is still at least one empty required field.
          $response = new RedirectResponse($edit_page);
          $event->setResponse($response);
        }
      }
      else {
        $config_factory = \Drupal::configFactory();
        $config = $config_factory->get('ain_misc.settings');
        $plan_visit_nid = ($config->get('ain_misc_plan_visit')) ? $config->get('ain_misc_plan_visit') : 0;
        if ($request->attributes->get('node') && $request->attributes->get('node')->id() == $plan_visit_nid) {
          // Exclude anonymous users and user 1 from the redirect.
          $nids = \Drupal::entityQuery('node')
            ->condition('type', 'visit_plan')
            ->condition('status', 0)
            ->condition('uid', $uid)
            ->condition('field_visit_date', date('Y-m-d H:i:s'), '>')
            ->execute();
          $visits_count = count($nids);
          if ($visits_count) {
            // Redirect to the most recent visit planned by the current user.
            $nids = array_reverse($nids, FALSE);
            $redirect_url = Url::fromUri('entity:node/' . $nids[0]);
            $response = new RedirectResponse($redirect_url->toString());
            $event->setResponse($response);
          }
        }
      }
    }
  }

  /**
   * Check url to exclude from redirecting.
   *
   * @param string $url
   *   An array containing keywords to exclude from redirecting if were in the url.
   */
  public function testUrl($url) {
    $exclude = FALSE;
    $excluded_array = array(
      'ajax_form',
      'iframe',
      'user/logout',
      'ar/user',
      '/node/'
    );

    foreach ($excluded_array as $value) {
      if (strpos($url, $value)) {
        $exclude = TRUE;
      }
    }

    return $exclude;
  }

}

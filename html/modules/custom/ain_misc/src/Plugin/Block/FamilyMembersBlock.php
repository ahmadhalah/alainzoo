<?php
/**
 * @file
 * Displays Family Members form as a block
 */
 
namespace Drupal\ain_misc\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'Secondary menu block' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "family_members_block",
 *   admin_label = @Translation("My Family"),
 *   category = @Translation("User")
 * )
 */
class FamilyMembersBlock extends BlockBase {
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\ain_misc\Form\FamilyMembers');
    return $form;
  }
}

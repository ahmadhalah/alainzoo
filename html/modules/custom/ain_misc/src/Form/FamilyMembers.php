<?php

namespace Drupal\ain_misc\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * A relatively simple AJAX demonstration form.
 */
class FamilyMembers extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajax_family_members';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Disable caching for the form.
    $form['#cache'] = ['max-age' => 0];
    $values = $form_state->getUserInput();

    // Make sure the block is executed from user profile.
    $route_match = \Drupal::routeMatch();
    if (in_array($route_match->getRouteName(), array('entity.user.canonical', 'entity.user.edit_form'))) {

      // Get the user form.
      $user = \Drupal::routeMatch()->getParameter('user');

      $form['container'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'added-members-container'],
      ];

      // Add New Family form.
      $form['container']['add-member'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['add-member-wrapper']],
      ];

      $form['container']['add-member']['icon'] = [
        '#type' => 'markup',
        '#markup' => '<div class="col-sm-1 family-icon"></div>'
      ];

      $form['container']['add-member']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#title_display' => 'invisible',
        '#required' => true,
        '#attributes' => ['placeholder' => $this->t('Name')],
        '#prefix' => '<div class="col-sm-3">',
        '#suffix' => '</div>'
      ];

      $form['container']['add-member']['relative'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Relative'),
        '#title_display' => 'invisible',
        '#default_value' => (isset($values['relative'])) ? $values['relative'] : '',
        '#required' => true,
        '#attributes' => ['placeholder' => $this->t('Relative')],
        '#prefix' => '<div class="col-sm-3">',
        '#suffix' => '</div>'
      ];

      $form['container']['add-member']['gender'] = [
        '#type' => 'select',
        '#title' => $this->t('Gender'),
        '#title_display' => 'invisible',
        '#required' => true,
        '#prefix' => '<div class="col-sm-3">',
        '#suffix' => '</div>',
        '#options' => [
          '' => $this->t('- Gender -'),
          'm' => $this->t('Male'),
          'f' => $this->t('Female')
        ],
      ];

      $form['container']['add-member']['submit'] = [
        '#type' => 'submit',
        '#submit' => ['::addSubmit'],
        '#limit_validation_errors' => [],
        '#value' => $this->t('Add'),
        '#name' => 'add',
        '#prefix' => '<div class="col-sm-2">',
        '#suffix' => '</div>',
        '#ajax' => [
          'callback' => '::addCallback',
          'wrapper' => 'added-members-container',
        ],
      ];

      // Check if the user submit the form to determine add or remove.
      if ($form_state->getTriggeringElement()) {
        $trigger_element = $form_state->getTriggeringElement()['#value']->__toString();
        $form = $form_state->getCompleteForm();

        // Set default value in case the validation failed.
        $form['container']['add-member']['relative']['#value'] = (isset($values['relative'])) ? $values['relative'] : '';
        $form['container']['add-member']['gender']['#value'] = (isset($values['gender'])) ? $values['gender'] : '';
        $form['container']['add-member']['name']['#value'] = (isset($values['name'])) ? $values['name'] : '';

        switch ($trigger_element) {

          case 'Add':
          case 'إضافة':
            if ($this->validateMember($values)) {
              $member = $this->createFamilyMemberNode($values, $user->id());
              if ($member) {
                $family_members = $user->field_family_member->getValue();
                $family_members[] = ['target_id' => $member->id()];
                $user->field_family_member->setValue($family_members);
                $user->save();

                // Add new family member row to form.
                $this->createFamilyMemberFormRow($form, $member);
                $form['container']['add-member']['name']['#value'] = '';
                $form['container']['add-member']['relative']['#value'] = '';
                $form['container']['add-member']['gender']['#value'] = '';
              }
            }
            break;

          case 'Remove':
          case 'حذف':
            $trigger_element = $form_state->getTriggeringElement()['#parents'][0];
            $member_id = str_replace('remove-', '', $trigger_element);
            $this->deleteMemberFromUser($member_id, $user);
            unset($form['container']['member-' . $member_id]);
            break;
        }
      }
      else {
        // Load the saved family members.
        $members = $user->get('field_family_member')->getValue();
        foreach ($members as $member_refference) {
          $this->createFamilyMemberFormRow($form, $member_refference['target_id']);
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function addSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $values = $form_state->getUserInput();
    if (!$this->validateMember($values)) {
      drupal_set_message($this->t('Name, Relative and Gender are required.'), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Callback for Adding family member.
   *
   */
  public function addCallback(array &$form, FormStateInterface $form_state) {
    return $form['container'];
  }

  /**
   * Callback for Adding family member.
   *
   */
  public function deleteCallback(array &$form, FormStateInterface $form_state) {
    return $form['container'];
  }

  public function createFamilyMemberNode($values, $uid) {
    if (isset($values['name']) && isset($values['relative']) && isset($values['gender'])) {
      $member = Node::create([
          'type' => 'family_member',
          'title' => $values['name'],
          'field_text' => $values['relative'],
          'field_gender' => $values['gender'],
          'uid' => $uid
      ]);

      $member->save();
      return $member;
    }
    else {
      return null;
    }
  }

  public function createFamilyMemberFormRow(array &$form, $member) {
    if (!$member instanceof Node) {
      $member = Node::load($member);
    }
    $id = $member->id();
    $name = $member->getTitle();
    $relative = $member->get('field_text')->getValue()[0]['value'];
    $gender = $member->get('field_gender')->getValue()[0]['value'];
    $gender_label = ($gender == 'f' ) ? $this->t('Female') : (($gender == 'm' ) ? $this->t('Male') : '');

    $row = '<div class="col-sm-1 sex-' . $gender . '"></div>';
    $row .= '<div class="col-sm-3">' . $name . '</div>';
    $row .= '<div class="col-sm-3">' . $relative . '</div>';
    $row .= '<div class="col-sm-3">' . $gender_label . '</div>';

    $member_row = [];

    $member_row['member-' . $id] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'member-' . $id],
      '#parents' => array('container'),
      '#prefix' => '<div class="family-member">'
    ];

    $member_row['member-' . $id]['details'] = [
      '#type' => 'markup',
      '#markup' => $row,
    ];

    $member_row['member-' . $id]['remove-' . $id] = [
      '#type' => 'submit',
      '#submit' => ['::removeSubmit'],
      '#limit_validation_errors' => [],
      '#value' => $this->t('Remove'),
      '#name' => 'remove-' . 'member-' . $id,
      '#prefix' => '<div class="col-sm-2">',
      '#suffix' => '</div></div>',
      '#ajax' => [
        'callback' => '::deleteCallback',
        'wrapper' => 'added-members-container',
      ],
    ];

    $form['container'] = $form['container'] + $member_row;
  }

  public function deleteMemberFromUser($mid, $user) {
    // Remove member form user entity refference.
    $user->get('field_family_member')->setValue($this->removeMemberFromUser($user->field_family_member->getValue(), $mid));
    $user->save();
  }

  function removeMemberFromUser($field, $mid) {
    foreach ($field as $key => $member) {
      if ($member['target_id'] == $mid) {
        unset($field[$key]);
      }
    }
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function validateMember($values) {
    $fields = ['name', 'relative', 'gender'];
    foreach ($fields as $field) {
      if ($values[$field] == '') {
        return false;
      }
    }
    return true;
  }

}

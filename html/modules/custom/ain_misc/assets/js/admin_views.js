(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.adminViews = {
    attach: function (context, settings) {
      setInterval(function () {
        var viewsToRefresh = ['.view-display-id-sos_requests', '.view-display-id-shuttle_requests'];
        $(viewsToRefresh).each(function () {
          var adminView = $($(this));
          if (adminView.length) {
            adminView.trigger('RefreshView');
          }
        });
      }, 30000);
    }
  };
}
)(window.jQuery, window._, window.Drupal, window.drupalSettings, window.localStorage);

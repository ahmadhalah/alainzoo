<?php

namespace Drupal\ain_services\Controller;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\user\Controller\UserAuthenticationController;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../../includes/ain_api_utilities.inc';

/**
 * Provides controllers for login, login status and logout via HTTP requests.
 */
class AinUserAuthenticationController extends UserAuthenticationController {

  use AinResponseResourceTrait;

  /**
   * {@inheritdoc}
   */
  public function resetPassword(Request $request) {
    $format = $this->getRequestFormat($request);

    $content = $request->getContent();
    $credentials = $this->serializer->decode($content, $format);

    $username = $credentials['username'];

    if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
      $user = user_load_by_mail($username);
      if ($user) {
        $username = $user->getAccountName();
      }
    }

    // Check if a name or mail is provided.
    if (!isset($username)) {
      $message = 'Missing mobile number or email address.';
      return $this->ain_response('failed', $message, 400);
    }

    // Load by name if provided.
    if (isset($username)) {
      $users = $this->userStorage->loadByProperties(['name' => trim($username)]);
    }

    /** @var \Drupal\Core\Session\AccountInterface $account */
    $account = reset($users);
    if ($account && $account->id()) {
      if ($this->userIsBlocked($account->getAccountName())) {
        $message = 'The user has not been activated or is blocked.';
        return $this->ain_response('failed', $message, 400);
      }

      // Send the password reset email.
      $mail = _user_mail_notify('password_reset', $account, $account->getPreferredLangcode());
      if (empty($mail)) {
        $message = 'Unable to send email. Contact the site administrator if the problem persists.';
        return $this->ain_response('failed', $message, 400);
      }
      else {
        $message = 'Password reset instructions is sent to';
        $messages = [
          'en' => $message . ': ' . $account->getEmail(),
          'ar' => __translate_to_ar($message) . ': ' . $account->getEmail(),
          'code' => 200
        ];
        return $this->ain_response_multiple('success', $messages, 200);
      }
    }

    // Error if no users found with provided name or mail.
    $message = 'Unrecognized mobile number or email address.';
    return $this->ain_response('failed', $message, 400);
  }

  /**
   * {@inheritdoc}
   */
  public function login(Request $request) {
    $format = $this->getRequestFormat($request);

    $content = $request->getContent();
    $credentials = $this->serializer->decode($content, $format);

    $pass = $credentials['pass'];
    $username = $credentials['username'];

    if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
      $user = user_load_by_mail($username);
      if ($user) {
        $username = $user->getAccountName();
      }
    }

    if (!isset($pass)) {
      $message = 'Missing password.';
      return $this->ain_response('failed', $message, 400);
    }

    if (!isset($username)) {
      $message = 'Missing mobile number or email address.';
      return $this->ain_response('failed', $message, 400);
    }

    $this->floodControl($request, $username);

    if ($this->userIsBlocked($username)) {
      $message = 'The user has not been activated or is blocked.';
      return $this->ain_response('failed', $message, 400);
    }

    if ($uid = $this->userAuth->authenticate($username, $pass)) {
      $this->flood->clear('user.http_login', $this->getLoginFloodIdentifier($request, $username));
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->userStorage->load($uid);
      $this->userLoginFinalize($user);

      // Send basic metadata about the logged in user.
      $response_data = [];
      if ($user->get('uid')->access('view', $user)) {
        $response_data['current_user']['uid'] = $user->id();
      }
      if ($user->get('roles')->access('view', $user)) {
        $response_data['current_user']['roles'] = $user->getRoles();
      }
      if ($user->get('name')->access('view', $user)) {
        $response_data['current_user']['name'] = $user->getAccountName();
      }
      $response_data['csrf_token'] = $this->csrfToken->get('rest');

      $logout_route = $this->routeProvider->getRouteByName('user.logout.http');
      // Trim '/' off path to match \Drupal\Core\Access\CsrfAccessCheck.
      $logout_path = ltrim($logout_route->getPath(), '/');
      $response_data['logout_token'] = $this->csrfToken->get($logout_path);

      $message = 'You have been successfully logged in.';
      return $this->ain_response('success', $message, 200, $response_data);
    }

    $flood_config = $this->config('user.flood');
    if ($identifier = $this->getLoginFloodIdentifier($request, $username)) {
      $this->flood->register('user.http_login', $flood_config->get('user_window'), $identifier);
    }
    // Always register an IP-based failed login event.
    $this->flood->register('user.failed_login_ip', $flood_config->get('ip_window'));
    $message = 'Unrecognized mobile number, email address or password.';
    return $this->ain_response('failed', $message, 400);
  }

  /**
   * {@inheritdoc}
   */
  public function logout() {
    $user = $this->currentUser();
    $this->userLogout();
    $message = 'You have been successfully logged out.';
    $messages = [
        'en' => $user->getAccountName() . ': ' . $message,
        'ar' => $user->getAccountName() . ': ' . __trasnlate_to_ar($message),
        'code' => 200
      ];
    return $this->ain_response_multiple('success', $messages, 200);
  }

}

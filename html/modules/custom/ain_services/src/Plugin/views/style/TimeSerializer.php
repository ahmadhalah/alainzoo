<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\TimeSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Serialization\Json;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "time_serializer",
 *   title = @Translation("Time Serializer"),
 *   help = @Translation("Serializes views row data using the TimeSerializer component."),
 *   display_types = {"data"}
 * )
 */
class TimeSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $langcode = \Drupal::request()->query->get('langcode');
    if (!isset($langcode)) {
      $langcode = 'en';
    }
    $rows = [];

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);

      $type = $rendered_row['type'];
      switch ($type) {
        case 'happening_now':
          $event_times = explode('\n', $rendered_row['times']);
          unset($rendered_row['times']);
          unset($rendered_row['date']);
          foreach ($event_times as $event_time) {
            $event_day_time = explode(',', $event_time);
            $event_day_int = trim($event_day_time[0]);
            $event_day_times = trim($event_day_time[1]);
            if ($event_day_times == 0) {
              continue;
            }
            $current_date = \Drupal::time()->getCurrentTime();
            $current_time = DrupalDateTime::createFromTimestamp($current_date, drupal_get_user_timezone());
            $current_day_int = DateHelper::dayOfWeek($current_time);

            if ($event_day_int - $current_day_int !== 1) {
              continue;
            }

            $time_value = [];
            $times_array = explode('|', $event_day_times);

            foreach ($times_array as $time_array) {
              $time = explode('-', $time_array);
              $start_time = DrupalDateTime::createFromTimestamp(strtotime($time[0]), drupal_get_user_timezone());
              $end_time = DrupalDateTime::createFromTimestamp(strtotime($time[1]), drupal_get_user_timezone());

              if ($current_time >= $start_time && $current_time <= $end_time) {

                $time_value[] = [
                  'start_time' => $time[0],
                  'end_time' => $time[1]
                ];
              }
            }

            $time_schedule = ['schedule' => []];

            if (count($time_value)) {
              $time_schedule = ['schedule' =>
                [
                  'times' => $time_value
                ]
              ];
              $rows[] = $rendered_row + $time_schedule;
            }
          }

          break;

        case 'events':
          $event_dates = Json::decode($rendered_row['date']);
          unset($rendered_row['times']);
          unset($rendered_row['date']);

          $time_value = [];
          foreach ($event_dates as $event_date) {
            $start_date_time = new \DateTime($event_date['start_date'], new \DateTimeZone('GMT'));
            $start_date_time->setTimezone(new \DateTimeZone(drupal_get_user_timezone()));
            $end_date_time = new \DateTime($event_date['end_date'], new \DateTimeZone('GMT'));
            $end_date_time->setTimezone(new \DateTimeZone(drupal_get_user_timezone()));

            $start_day = $start_date_time->format('Y-m-d');
            $start_time = $start_date_time->format('H:i');
            $end_day = $end_date_time->format('Y-m-d');
            $end_time = $end_date_time->format('H:i');

            $current_date = \Drupal::time()->getCurrentTime();
            $current_time = DrupalDateTime::createFromTimestamp($current_date);


            $start_day_raw = DrupalDateTime::createFromTimestamp(strtotime($start_day), drupal_get_user_timezone());
            $start_time_raw = DrupalDateTime::createFromTimestamp(strtotime($start_time), drupal_get_user_timezone());
            $end_day_raw = DrupalDateTime::createFromTimestamp(strtotime($end_day), drupal_get_user_timezone());
            $end_time_raw = DrupalDateTime::createFromTimestamp(strtotime($end_time), drupal_get_user_timezone());
            if ($current_time >= $start_day_raw && $current_time <= $end_day_raw && $current_time >= $start_time_raw && $current_time <= $end_time_raw) {
              $time_value[] = [
                'start_time' => $start_time,
                'end_time' => $end_time
              ];
            }
          }

          $time_schedule = ['schedule' => []];

          if (count($time_value)) {
            $time_schedule = ['schedule' =>
              [
                'times' => $time_value
              ]
            ];
            $rows[] = $rendered_row + $time_schedule;
          }

          break;

        case 'treasure_hunt':
          $event_dates = Json::decode($rendered_row['date']);
          unset($rendered_row['times']);
          unset($rendered_row['date']);
          unset($rendered_row['type']);

          $time_value = [];

          foreach ($event_dates as $event_date) {
            $timezone = DATETIME_STORAGE_TIMEZONE;
            $start_date = new DrupalDateTime($event_date['start_date']);
            $end_date = new DrupalDateTime($event_date['end_date']);
            $current_time = new DrupalDateTime('now');

            if(($start_date <= $current_time) && ($current_time  <= $end_date)) {
              $start_time = new DrupalDateTime($current_time->format('Y-m-d') . ' ' . $start_date->format('H:i'));
              $end_time = new DrupalDateTime($current_time->format('Y-m-d') . ' ' . $end_date->format('H:i'));
              if (($start_time <= $current_time) && ($current_time <= $end_time)) {
                $rows[] = $rendered_row;
              }
            }
          }
          break;
      }
    }

    return $this->serializer->serialize($rows, 'json');
  }

}

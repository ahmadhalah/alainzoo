<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\DateSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "date_serializer",
 *   title = @Translation("Date Serializer"),
 *   help = @Translation("Serializes views row data using the DateSerializer component."),
 *   display_types = {"data"}
 * )
 */
class DateSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = array();

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      if ($rendered_row['date'] == '') {
        continue;
      }
      $event_dates = explode('\n', $rendered_row['date']);
      foreach ($event_dates as $event_date) {
        $date = explode(',', $event_date);
        $rows[] = [
          'start_date' => $this->get_timpstamp($date[0]),
          'end_date' => $this->get_timpstamp($date[1])
        ];
      }
    }

    return $this->serializer->serialize($rows, 'json');
  }

  /**
   * Return Timestamp format for date.
   */
  public function get_timpstamp($date) {

    $userTimezone = new \DateTimeZone(drupal_get_user_timezone());
    $gmtTimezone = new \DateTimeZone('GMT');
    $myDateTime = new \DateTime($date, $gmtTimezone);

    $offset = $userTimezone->getOffset($myDateTime);
    $myInterval = \DateInterval::createFromDateString((string)$offset . 'seconds');
    $myDateTime->add($myInterval);
    return  $myDateTime->format('Y-m-d\TH:i:s');
  }

}


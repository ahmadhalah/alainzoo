<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinEntityResourceValidationTrait;
use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\user\Plugin\rest\resource\UserRegistrationResource;
use Drupal\user\UserInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Represents user registration as a resource.
 *
 * @RestResource(
 *   id = "ain_user_registration",
 *   label = @Translation("Ain User Registration."),
 *   serialization_class = "Drupal\user\Entity\User",
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/user/register",
 *   },
 * )
 */
class AinUserRegistrationResource extends UserRegistrationResource {

  use AinEntityResourceValidationTrait;
  use AinResponseResourceTrait;

  /**
   * Responds to user registration POST request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function post(UserInterface $account = NULL) {

    if ($account === NULL) {
      $message = 'No user account data for registration received.';
      return $this->ain_response('failed', $message, 400);
    }

    // POSTed user accounts must not have an ID set, because we always want to
    // create new entities here.
    if (!$account->isNew()) {
      $message = 'An ID has been set and only new user accounts can be registered.';
      return $this->ain_response('failed', $message, 400);
    }

    // Verify that the current user can register a user account.
    if ($this->userSettings->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY) {
      $message = 'You cannot register a new user account.';
      return $this->ain_response('failed', $message, 403);
    }

    if (!$this->userSettings->get('verify_mail')) {
      if (empty($account->getPassword())) {
        // If no e-mail verification then the user must provide a password.
        $message = 'No password provided.';
        return $this->ain_response('failed', $message, 422);
      }
    }
    else {
      if (!empty($account->getPassword())) {
        // If e-mail verification required then a password cannot provided.
        // The password will be set when the user logs in.
        $message = 'A Password cannot be specified. It will be generated on login.';
        return $this->ain_response('failed', $message, 422);
      }
    }

    // Only activate new users if visitors are allowed to register and no email
    // verification required.
    if ($this->userSettings->get('register') == USER_REGISTER_VISITORS && !$this->userSettings->get('verify_mail')) {
      $account->activate();
    }
    else {
      $account->block();
    }

    $this->checkEditFieldAccess($account);

    $username = $account->getAccountName();

    if (!is_numeric($username)) {
      $message = 'Invalid mobile number.';
      return $this->ain_response('failed', $message, 422);
    }

    $date_of_birth = $account->get('field_date_of_bi')->value;
    $now = new DrupalDateTime('now');
    $now_value = $now->render();
    if (isset($date_of_birth) && $date_of_birth > $now_value) {
      $message = 'Date of birth should be in the past.';
      return $this->ain_response('failed', $message, 422);
    }

    $full_name = $account->get('field_name')->value;
    if (isset($full_name) && !preg_match('~^[\p{L}\p{Z}]+$~u', $full_name)) {
      $message = 'Invalid Name, name should not include numbers.';
      return $this->ain_response('failed', $message, 422);
    }

    // Make sure that the user entity is valid (email and name are valid).
    $messages = $this->validate($account);
    if (count($messages)) {
      return $this->ain_response_multiple('failed', $messages, 422);
    }

    $facebook_token = $account->get('field_facebook')->value;

    if (isset($facebook_token)) {
      $client = \Drupal::httpClient();
      try {
        $request = $client->request('GET', 'https://graph.facebook.com/me/?access_token=' . $facebook_token);
        $status = $request->getStatusCode();
        if ($request->getStatusCode() == 200) {
          $response = json_decode($request->getBody());

          // Check if user exist on website.
          $query = \Drupal::database()->select('social_auth', 'social_auth');
          $query->fields('social_auth', ['user_id']);
          $query->condition('social_auth.provider_user_id', $response->id);
          $query->condition('social_auth.plugin_id', 'social_auth_facebook');
          $user_id = $query->execute()->fetchField();

          if ($user_id) {
            $message = 'Facebook user is already exist';
            return $this->ain_response('failed', $message, 422);
          }
        }
      }
      catch (RequestException $e) {
        $message = 'Error connecting to facebook.';
        return $this->ain_response('failed', $message, 422);
      }
    }

    // Create the account.
    $account->save();

    if ($facebook_token) {
      $values = [
        'user_id' => $account->id(),
        'plugin_id' => 'social_auth_facebook',
        'provider_user_id' => $response->id,
        'token' => $facebook_token,
        'additional_data' => ' ',
      ];

      $user_info = \Drupal::entityTypeManager()->getStorage('social_auth')->create($values);
      $user_info->save();
    }

    $message = 'User account is created successfully, Starting points have credited to your account!';
    return $this->ain_response('success', $message, 201, $account->toArray());
  }

}

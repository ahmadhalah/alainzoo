<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_visit_plan_resource",
 *   label = @Translation("Ain Visit Plan"),
 *   uri_paths = {
 *     "canonical" = "/visit_plan/create",
 *     "https://www.drupal.org/link-relations/create" = "/visit_plan/create"
 *   }
 * )
 */
class AinVisitPlanResource extends ResourceBase {

  use AinResponseResourceTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * An estimated visit duration.
   *
   * @integer
   */
  protected $estimatedDuration = 0;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function post(array $data) {
    $user_id = $this->currentUser->id();

    $args = [$user_id];
    $view = Views::getView('api_services');
    $view->setArguments($args);
    $view->setDisplay('my_plan_id');
    $view->preExecute();
    $view->execute();
    $content = $view->render();
    $data_string = $content['#markup']->jsonSerialize();
    $data_array = Json::decode($data_string);

    $plan_id = $data_array[0]['id'];
    if ($plan_id) {
      $message = 'You are allowed to create only one visit plan.';
      return $this->ain_response('failed', $message, 422);
    }

    // Check for event date submission.
    if (!isset($data['visit_date'])) {
      $message = 'Please enter your visit date.';
      return $this->ain_response('failed', $message, 422);
    }
    else {
      // verify a valid future visit date.
      $visit_date = $data['visit_date'];
      $now = new DrupalDateTime('now');
      $now_value = $now->format('Y-m-d');
      if (!$this->validateDate($visit_date) || $visit_date < $now_value) {
        $message = 'Visit date should be a valid date in the future.';
        return $this->ain_response('failed', $message, 422);
      }
    }

    // Check for recommended plan id submission.
    if (isset($data['recommended_plan'])) {
      $recommended_plan_id = $data['recommended_plan'];

      // Check for numeric recommended plan id.
      if (!is_numeric($recommended_plan_id)) {
        $message = 'Invalid recommended plan, it should be a valid content id.';
        return $this->ain_response('failed', $message, 422);
      }

      // Check for valid visit plan content type.
      $recommended_plan = Node::load($recommended_plan_id);
      if (!$recommended_plan || $recommended_plan->type->target_id !== 'visit_plan') {
        $message = 'Invalid recommended plan.';
        return $this->ain_response('failed', $message, 422);
      }

      if (!$recommended_plan || $recommended_plan->field_is_recommended->value != 1) {
        $message = 'Invalid recommended plan.';
        return $this->ain_response('failed', $message, 422);
      }

      // Create new visit plan based on the provided recommended plan.
      // Load fields values of the selected plan.
      $visit_date = strtotime($data['visit_date']);
      $recommended_plan_title = $recommended_plan->get('title')->getValue();
      $title = $recommended_plan_title[0]['value'] . ' - ' . date('l', $visit_date);
      $recommended_plan_attractions = $recommended_plan->get('field_attraction')->getValue();
      $recommended_plan_animals = $recommended_plan->get('field_animal')->getValue();
      $recommended_plan_experiences = $recommended_plan->get('field_experience_reference')->getValue();
      $recommended_plan_events = $recommended_plan->get('field_what_s_new')->getValue();
      $recommended_plan_details = $recommended_plan->get('field_plan_details')->getValue();
      $recommended_plan_image = $recommended_plan->get('field_plan_image')->getValue();
      $recommended_plan_items = $recommended_plan->get('field_items')->getValue();
      $recommended_plan_suggested_tickets_en = ($recommended_plan->get('field_suggested_ticket_en')->getValue()) ? $recommended_plan->get('field_suggested_ticket_en')->getValue() : 0;
      $recommended_plan_suggested_tickets_ar = ($recommended_plan->get('field_suggested_ticket_ar')->getValue()) ? $recommended_plan->get('field_suggested_ticket_ar')->getValue() : 0;
      $recommended_plan_suggested_tickets = $recommended_plan->get('body')->getValue();
      $recommended_plan_duration = $recommended_plan->get('field_duration')->getValue();

      // Create new visit plan from recommended plan.
      $node = Node::create(
          [
            'title' => $title,
            'field_plan' => $recommended_plan_id,
            'type' => 'visit_plan',
            'field_plan_details' => $recommended_plan_details,
            'field_plan_image' => $recommended_plan_image,
            'field_attraction' => $recommended_plan_attractions,
            'field_animal' => $recommended_plan_animals,
            'field_experience_reference' => $recommended_plan_experiences,
            'field_what_s_new' => $recommended_plan_events,
            'field_items' => $recommended_plan_items,
            'field_suggested_ticket_en' => $recommended_plan_suggested_tickets_en,
            'field_suggested_ticket_ar' => $recommended_plan_suggested_tickets_ar,
            'body' => $recommended_plan_suggested_tickets,
            'field_duration' => $recommended_plan_duration,
            'field_visit_date' => date('Y-m-d', $visit_date),
            'uid' => $this->currentUser->id(),
            'status' => 0,
          ]
      );
      $node->save();

      // Check for successfully creation of visit plan.
      if ($node) {
        $message = 'visit plan was created successfully';
        return $this->ain_response('success', $message, 201, $node->id());
      }
      else {
        $message = 'An error occured, please try again.';
        return $this->ain_response('failed', $message, 422);
      }
    }
    else {
      // Check for title submission.
      if (!isset($data['title'])) {
        $message = 'Please enter your visit title.';
        return $this->ain_response('failed', $message, 422);
      }

      // Check for attractions submission.
      if (!isset($data['attractions']) || !is_array($data['attractions'])) {
        $message = 'Please select at least one attraction.';
        return $this->ain_response('failed', $message, 422);
      }
      else {
        // Validate array values to be integers and a valid attractions.
        if (!$this->validateArrayValues('attraction', $data['attractions'])) {
          $message = 'Please select valid attractions.';
          return $this->ain_response('failed', $message, 422);
        }
      }

      // Check for animals submission.
      if (!isset($data['animals']) || !is_array($data['animals'])) {
        $message = 'Please select at least one animal.';
        return $this->ain_response('failed', $message, 422);
      }
      else {
        // Validate array values to be integers and a valid animals.
        if (!$this->validateArrayValues('animals', $data['animals'])) {
          $message = 'Please select valid animals.';
          return $this->ain_response('failed', $message, 422);
        }
      }

      // Check for experiences submission.
      if (!isset($data['experiences']) || !is_array($data['experiences'])) {
        $message = 'Please select at least one experience.';
        return $this->ain_response('failed', $message, 422);
      }
      else {
        // Validate array values to be integers and a valid attractions.
        if (!$this->validateArrayValues('experience', $data['experiences'])) {
          $message = 'Please select valid experiences.';
          return $this->ain_response('failed', $message, 422);
        }
      }

      // Check for events submission.
      if (isset($data['events'])) {
        $events = $data['events'];
        // Validate array values to be integers and a valid attractions.
        if (!is_array($data['events']) || !$this->validateArrayValues('events', $data['events'])) {
          $message = 'Please select valid events.';
          return $this->ain_response('failed', $message, 422);
        }
      }
      else {
        $events = [];
      }

      // Load configurations.
      $config_factory = \Drupal::configFactory();
      $config = $config_factory->get('ain_misc.settings');

      // Create new visit plan from customized plan.
      $visit_date = strtotime($data['visit_date']);
      $title = $data['title'] . ' - ' . date('l', $visit_date);
      $attractions = $data['attractions'];
      $animals = $data['animals'];
      $experiences = $data['experiences'];
      $suggested_tickets_en = 0;
      $suggested_tickets_ar = 0;
      $suggested_tickets_table = [];
      $message = '';

      // Suggested tickets
      $attractionsMap = [];
      $attractionsMap['szdlc'] = ($config->get('ain_misc_attraction_szdlc')) ? $config->get('ain_misc_attraction_szdlc') : 0;
      $attractionsMap['safari'] = ($config->get('ain_misc_attraction_safari')) ? $config->get('ain_misc_attraction_safari') : 0;

      if (count($animals)) {
        if (count($attractions)) {
          if (in_array($attractionsMap['szdlc'], $attractions) && in_array($attractionsMap['safari'], $attractions)) {
            // Animals and SZDLC and Safari - Safari Explorer.
            $suggested_tickets_en = ($config->get('ain_misc_ticket_safari_explorer_en')) ? $config->get('ain_misc_ticket_safari_explorer_en') : 0;
            $suggested_tickets_ar = ($config->get('ain_misc_ticket_safari_explorer_ar')) ? $config->get('ain_misc_ticket_safari_explorer_ar') : 0;
          }
          else {
            if (in_array($attractionsMap['szdlc'], $attractions)) {
              // Animals and SZDLC - Explorer Ticket.
              $suggested_tickets_en = ($config->get('ain_misc_ticket_explorer_ticket_en')) ? $config->get('ain_misc_ticket_explorer_ticket_en') : 0;
              $suggested_tickets_ar = ($config->get('ain_misc_ticket_explorer_ticket_ar')) ? $config->get('ain_misc_ticket_explorer_ticket_ar') : 0;
            }
            if (in_array($attractionsMap['safari'], $attractions)) {
              // Animals and Safari - Safari Entertainer.
              $suggested_tickets_en = ($config->get('ain_misc_ticket_safari_entertainer_en')) ? $config->get('ain_misc_ticket_safari_entertainer_en') : 0;
              $suggested_tickets_ar = ($config->get('ain_misc_ticket_safari_entertainer_ar')) ? $config->get('ain_misc_ticket_safari_entertainer_ar') : 0;
            }
          }
        }
        else {
          // Animals Only - Entertainer Ticket.
          $suggested_tickets_en = ($config->get('ain_misc_ticket_entertainer_ticket_en')) ? $config->get('ain_misc_ticket_entertainer_ticket_en') : 0;
          $suggested_tickets_ar = ($config->get('ain_misc_ticket_entertainer_ticket_ar')) ? $config->get('ain_misc_ticket_entertainer_ticket_ar') : 0;
        }
      }

      $args = ['en'];
      $view = Views::getView('api_services');
      $view->setArguments($args);
      $view->setDisplay('suggested_tickets');
      $view->preExecute();
      $view->execute();
      $content = $view->render();
      $data_string = $content['#markup']->jsonSerialize();
      $data_array = Json::decode($data_string);

      foreach ($data_array as $data_item) {
        if ($data_item['nid'] == $suggested_tickets_en) {
          $suggested_tickets_table = $data_item;
        }
      }

      if (count($suggested_tickets_table)) {
        $message = '<table class="table">';
        $message .= '<tr><td>Suggested ticket for Adult</td><td>' . $suggested_tickets_table['title'] . '</td><td>' . $suggested_tickets_table['adult'] . ' AED</td></tr>';
        $message .= '<tr><td>Suggested ticket for Child</td><td>' . $suggested_tickets_table['title'] . '</td><td>' . $suggested_tickets_table['child'] . ' AED</td></tr>';
        $message .= '</table>';
      }
      else {
        $message = '';
      }

      $node = Node::create(
          [
            'title' => $title,
            'field_plan' => 0,
            'type' => 'visit_plan',
            'field_attraction' => $attractions,
            'field_animal' => $animals,
            'field_experience_reference' => $experiences,
            'field_what_s_new' => $events,
            'field_visit_date' => date('Y-m-d', $visit_date),
            'uid' => $this->currentUser->id(),
            'field_duration' => $this->estimatedDuration,
            'field_suggested_ticket_en' => $suggested_tickets_en,
            'field_suggested_ticket_ar' => $suggested_tickets_ar,
            'body' => ['value' => $message, 'format' => 'basic_editor'],
            'status' => 0,
          ]
      );
      $node->save();

      // Check for successfully creation of visit plan.
      if ($node) {
        $message = 'visit plan was created successfully';
        return $this->ain_response('success', $message, 201, $node->id());
      }
      else {
        $message = 'An error occured, please try again.';
        return $this->ain_response('failed', $message, 422);
      }
    }
  }

  /*
   * Validate visit date in the format YYYY-MM-DD
   *
   * @param date $date
   *   The visit date.
   *
   * @return boolean
   *   If validation success returns true otherwise returns false.
   */

  public function validateDate($date) {
    if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $date, $matches)) {
      if (checkdate($matches[2], $matches[3], $matches[1])) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  /*
   * Validate array values to be all integers.
   *
   * @param array $date
   *   The array to be validated.
   *
   * @param string $type
   *   Content type.
   *
   * @return boolean
   *   If validation success returns true otherwise returns false.
   */

  public function validateArrayValues($type, $data) {
    foreach ($data as $value) {
      if (!is_numeric($value)) {
        return false;
      }
      $node = Node::load($value);
      if (!$node || $node->type->target_id !== $type) {
        return false;
      }
      else {
        $duration = isset($node->get('field_duration')->value) ? $node->get('field_duration')->value : 0;
        $this->estimatedDuration += $duration;
      }
    }
    return true;
  }

}

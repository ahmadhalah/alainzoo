<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_family_member_resource",
 *   label = @Translation("Ain Family Member"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/family_member",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/family_member"
 *   }
 * )
 */
class AinFamilyMemberResource extends ResourceBase {

  use AinResponseResourceTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function post(array $data) {

    // Check for name submission.
    if (!isset($data['name']) || !isset($data['relative']) || !isset($data['gender'])) {
      $message = 'Your request was not completed, due to error in sent data.';
      return $this->ain_response('failed', $message, 422);
    }
    else {
      // verify a valid gender value.
      $gender = $data['gender'];
      if (!in_array($gender, array('m', 'f'))) {
        $message = 'Invalid gender value, allowed values are: "m" or "f".';
        return $this->ain_response('failed', $message, 422);
      }
    }

    // Create new family member.
    $title = $data['name'];
    $relative = $data['relative'];
    $node = Node::create(
        [
          'title' => $title,
          'type' => 'family_member',
          'field_text' => $relative,
          'field_gender' => $gender,
          'uid' => $this->currentUser->id(),
        ]
    );
    $node->save();

    // Check for successfully creation of family member.
    if ($node) {
      $user = User::load($this->currentUser->id());
      $user_family_members = $user->field_family_member->getValue();
      $user_family_members[] = ['target_id' => $node->id()];
      $user->field_family_member->setValue($user_family_members);
      $user->save();

      $message = 'Family member is created successfully';
      return $this->ain_response('success', $message, 201, $node->id());
    }
    else {
      $message = 'An error occured, please try again.';
      return $this->ain_response('failed', $message, 422);
    }
  }

  public function delete(array $data) {
    if(isset($data['family_id'])) {
      $id = $data['family_id'];
      $user = User::load($this->currentUser->id());
      if($this->deleteMemberFromUser($id, $user)) {
        $message = 'Your family member was removed successfully.';
        return $this->ain_response('success', $message, 201); 
      }
    }
    $message = 'Your request was not completed, due to error in sent data.';
    return $this->ain_response('failed', $message, 422);
  }

  public function deleteMemberFromUser($mid, $user) {
    // Remove member form user entity refference.
    $family = $user->field_family_member->getValue();
    if($this->hasMember($family, $mid)) {
      $user->get('field_family_member')->setValue($this->removeMemberFromUser($family, $mid));
      $user->save();
      return true;
    }
    return false;
  }

  function removeMemberFromUser($family, $mid) {
    foreach ($family as $key => $member) {
      if ($member['target_id'] == $mid) {
        unset($family[$key]);
      }
    }
    return $family;
  }

  function hasMember($family, $mid) {
    foreach ($family as $key => $member) {
      if ($member['target_id'] == $mid) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

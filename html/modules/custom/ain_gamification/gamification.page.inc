<?php

/**
 * @file
 * Contains gamification.page.inc.
 *
 * Page callback for Gamification entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Gamification templates.
 *
 * Default template: gamification.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gamification(array &$variables) {
  // Fetch Gamification Entity Object.
  $gamification = $variables['elements']['#gamification'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

<?php

namespace Drupal\ain_gamification;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Gamification entity.
 *
 * @see \Drupal\ain_gamification\Entity\Gamification.
 */
class GamificationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ain_gamification\Entity\GamificationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished gamification entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published gamification entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit gamification entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete gamification entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add gamification entities');
  }

}

<?php

namespace Drupal\ain_gamification;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Gamification entities.
 *
 * @ingroup ain_gamification
 */
class GamificationListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['point'] = $this->t('Points');
    $header['cuid'] = $this->t('User');
    $header['created'] = $this->t('Created');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\ain_gamification\Entity\Gamification */
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $row['id'] = Link::createFromRoute(
      $this->t('Gamification') . ': ' .$entity->id(),
      'entity.gamification.canonical',
      ['gamification' => $entity->id()]
    );
    $row['type'] = $entity->field_type->value;
    $row['point'] = $entity->field_point->value;
    $row['cuid'] = ($entity->get('field_cuid')->getValue()) ? user_load($entity->get('field_cuid')->getValue()[0]['target_id'])->getUsername() : '';
    $row['created'] = format_date($entity->created->value, 'html');
    $row['status'] = ($entity->status->value) ? $this->t('Published') : $this->t('Unpublished');
    return $row + parent::buildRow($entity);
  }

}

<?php

namespace Drupal\ain_gamification\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Gamification entities.
 *
 * @ingroup ain_gamification
 */
interface GamificationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Gamification creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Gamification.
   */
  public function getCreatedTime();

  /**
   * Sets the Gamification creation timestamp.
   *
   * @param int $timestamp
   *   The Gamification creation timestamp.
   *
   * @return \Drupal\ain_gamification\Entity\GamificationInterface
   *   The called Gamification entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Gamification published status indicator.
   *
   * Unpublished Gamification are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Gamification is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Gamification.
   *
   * @param bool $published
   *   TRUE to set this Gamification to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ain_gamification\Entity\GamificationInterface
   *   The called Gamification entity.
   */
  public function setPublished($published);

}

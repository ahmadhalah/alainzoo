<?php

namespace Drupal\ain_gamification\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Gamification entity.
 *
 * @ingroup ain_gamification
 *
 * @ContentEntityType(
 *   id = "gamification",
 *   label = @Translation("Gamification"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ain_gamification\GamificationListBuilder",
 *     "views_data" = "Drupal\ain_gamification\Entity\GamificationViewsData",
 *     "translation" = "Drupal\ain_gamification\GamificationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\ain_gamification\Form\GamificationForm",
 *       "add" = "Drupal\ain_gamification\Form\GamificationForm",
 *       "edit" = "Drupal\ain_gamification\Form\GamificationForm",
 *       "delete" = "Drupal\ain_gamification\Form\GamificationDeleteForm",
 *     },
 *     "access" = "Drupal\ain_gamification\GamificationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ain_gamification\GamificationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gamification",
 *   data_table = "gamification_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer gamification entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "field_type" = "field_type",
 *     "field_point" = "field_point",
 *     "field_treasure_hunt" = "field_treasure_hunt",
 *     "action_id" = "action_id",
 *     "field_hint" = "field_hint",
 *     "uid" = "user_id",
 *     "field_cuid" = "field_cuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/gamification/{gamification}",
 *     "add-form" = "/admin/structure/gamification/add",
 *     "edit-form" = "/admin/structure/gamification/{gamification}/edit",
 *     "delete-form" = "/admin/structure/gamification/{gamification}/delete",
 *     "collection" = "/admin/structure/gamification",
 *   },
 *   field_ui_base_route = "gamification.settings"
 * )
 */
class Gamification extends ContentEntityBase implements GamificationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('field_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($string) {
    $this->set('field_type', $string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getActionID() {
    return $this->get('action_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActionID($string) {
    $this->set('action_id', $string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTreasureHunt() {
    return $this->get('field_treasure_hunt')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setTreasureHunt($string) {
    $this->set('field_treasure_hunt', $string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHint() {
    return $this->get('field_hint')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setHint($string) {
    $this->set('field_hint', $string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->get('field_cuid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setUser($cuid) {
    $this->set('field_cuid', $cuid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPoints() {
    return $this->get('field_point')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPoints($points) {
    $this->set('field_point', $points);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['field_point'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Points'))
      ->setSettings([
        'suffix' => ' Points',
      ])
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['action_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', false)
      ->setDisplayConfigurable('view', false);

    $fields['field_cuid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Admin'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDefaultValue(\Drupal::currentUser()->id())
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'));

    $fields['field_treasure_hunt'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Treasure hunt'))
      ->setCardinality(1)
      ->setSetting('target_type', 'node')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'treasure_hunt' => 'treasure_hunt'
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_hint'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Hint'))
      ->setCardinality(1)
      ->setSetting('target_type', 'node')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'hint' => 'hint'
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}

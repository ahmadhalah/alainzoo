<?php

namespace Drupal\ain_gamification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure GamificationAdminSettingsForm settings for this site.
 */
class GamificationAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ain_gamification_form_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ain_gamification.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('ain_gamification.settings');

    $form['ain_gamification_form_intro'] = [
      '#markup' => '<p>Configure points for each <em>Gamification</em> action.',
    ];

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Gamification Actions Points'),
      '#open' => TRUE,
    ];

    $form['general']['ain_gamification_registration'] = [
      '#type' => 'number',
      '#title' => $this->t('Registration'),
      '#step' => 1,
      '#description' => $this->t('Number of points the user is rewarded after completing <em>Registration</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_gamification_registration')) ? $config->get('ain_gamification_registration') : 0,
    ];

    $form['general']['ain_gamification_share_app'] = [
      '#type' => 'number',
      '#title' => $this->t('Sharing Application'),
      '#step' => 1,
      '#description' => $this->t('Number of points the user is rewarded after completing <em>Sharing Application</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_gamification_share_app')) ? $config->get('ain_gamification_share_app') : 0,
    ];

    $form['general']['ain_gamification_plan_visit'] = [
      '#type' => 'number',
      '#title' => $this->t('Planning Visit'),
      '#step' => 1,
      '#description' => $this->t('Number of points the user is rewarded after completing <em>Planning Visit</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_gamification_plan_visit')) ? $config->get('ain_gamification_plan_visit') : 0,
    ];

    $form['general']['ain_gamification_share_content'] = [
      '#type' => 'number',
      '#title' => $this->t('Sharing Content'),
      '#step' => 1,
      '#description' => $this->t('Number of points the user is rewarded after completing <em>Sharing Content</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_gamification_share_content')) ? $config->get('ain_gamification_share_content') : 0,
    ];

    $form['general']['ain_gamification_downloaded_game'] = [
      '#type' => 'number',
      '#title' => $this->t('Downloading a Game'),
      '#step' => 1,
      '#description' => $this->t('Number of points the user is rewarded after completing <em>Downloading a Game</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_gamification_downloaded_game')) ? $config->get('ain_gamification_downloaded_game') : 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ain_gamification.settings');
    $config
      ->set('ain_gamification_registration', $form_state->getValue('ain_gamification_registration'))
      ->set('ain_gamification_share_app', $form_state->getValue('ain_gamification_share_app'))
      ->set('ain_gamification_plan_visit', $form_state->getValue('ain_gamification_plan_visit'))
      ->set('ain_gamification_share_content', $form_state->getValue('ain_gamification_share_content'))
      ->set('ain_gamification_downloaded_game', $form_state->getValue('ain_gamification_downloaded_game'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

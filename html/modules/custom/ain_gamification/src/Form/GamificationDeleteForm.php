<?php

namespace Drupal\ain_gamification\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Gamification entities.
 *
 * @ingroup ain_gamification
 */
class GamificationDeleteForm extends ContentEntityDeleteForm {


}

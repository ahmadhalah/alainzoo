<?php

/**
 * @file
 * Contains \Drupal\ain_map\Controller\MapPageController.
 */
namespace Drupal\ain_map\Controller;

use Drupal\Core\Controller\ControllerBase;

class MapPageController extends ControllerBase {
  public function mapPage($latitude, $longitude) {

    $build['#attached']['library'][] = 'ain_map/ain_map';
    $build['#attached']['drupalSettings']['ain_map']['map'] = _get_map_settings_information();
    
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\ain_map\Form\mapForm');
    $build['map'] = [
      '#type' => 'markup',
      '#markup' => '<div id ="alain-map" latitude="' . $latitude . '" longitude="' . $longitude . '"> </div>',
    ];
    
    return $build;
  }
}

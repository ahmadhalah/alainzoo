<?php

/**
 * @file
 * Contains \Drupal\ain_map\Plugin\views\style\MapSerializer.
 */

namespace Drupal\ain_map\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "Map_serializer",
 *   title = @Translation("Map Serializer"),
 *   help = @Translation("Serializes views row data using the MapSerializer component."),
 *   display_types = {"data"}
 * )
 */
class MapSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = array();

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      $rows[] = [
        'type' => 'Feature',
        'properties' => [
          'id' => $rendered_row['id'],
          'area' => $rendered_row['area_id'],
          'category' => $rendered_row['category_id'],
          'name' => $rendered_row['name'],
          'type' => $rendered_row['type'],
          'icon' => $rendered_row['icon'],
          'thumbnail' => $rendered_row['thumbnail'],
          'path' => $rendered_row['path'],
          'body' => $rendered_row['details'],
        ],
        'geometry' => [
          'type' => 'Point',
          'coordinates' => [
            $rendered_row['longitude'],
            $rendered_row['latitude'],
          ],
        ],
      ];

      if(!empty($rendered_row['longitude2']) && !empty($rendered_row['latitude2'])) {
        $rows[] = [
          'type' => 'Feature',
          'properties' => [
            'id' => $rendered_row['id'],
            'area' => $rendered_row['area_id'],
            'category' => $rendered_row['category_id'],
            'name' => $rendered_row['name'],
            'type' => $rendered_row['type'],
            'icon' => $rendered_row['icon'],
            'thumbnail' => $rendered_row['thumbnail'],
            'path' => $rendered_row['path'],
            'body' => $rendered_row['details'],
          ],
          'geometry' => [
            'type' => 'Point',
            'coordinates' => [
              $rendered_row['longitude2'],
              $rendered_row['latitude2'],
            ],
          ],
        ];
      }

      $this->view->row_index = $row_index;
    }
    
    $result = [
      'type' => 'FeatureCollection',
      'features' => $rows
    ];   

    unset($this->view->row_index);
    return $this->serializer->serialize($result, 'json');
  }

}

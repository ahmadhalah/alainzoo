<?php

namespace Drupal\ain_map\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;

/**
 * Configure BsTour settings for this site.
 */
class mapForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'map_form';
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $map_data_content = $this->getValues('public_api_content', 'public_map_data');
    $map_categories = $this->getValues('public_api_taxonomy', 'public_animals_category', 'animal');
    $map_data = array_merge($map_data_content, $map_categories);

    $form['animals'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Animals'),
      '#options' => array_key_exists('animal',$map_data) ? $map_data['animal'] : [],
      '#attributes' => array('checked' => 'checked')
    );

    $form['experience'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Experiences'),
      '#options' => array_key_exists('experience',$map_data) ? $map_data['experience'] : [],
    );

    $form['venue'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Venues'),
      '#options' => array_key_exists('venue',$map_data) ? $map_data['venue'] : [],
    );

    $form['search'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#placeholder' => t('Search')
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

  function getValues($view_id, $display_id, $key=null) {
    $language_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
    
    $args = [$language_code];
    $view = Views::getView($view_id);
    $view->setArguments($args);
    $view->setDisplay($display_id);
    $view->preExecute();
    $view->execute();
    $content = $view->render();
    $data_string = $content['#markup']->jsonSerialize();
    $data = Json::decode($data_string);

    $return = array();

    foreach ($data as $val) {
      if ($key !== null) {
        $return[$key][$val['id']] = $val['name'];
      }
      else {
        $return[$val['type']][$val['id']] = $val['name'];
      }
    }

    return $return;
  }

}

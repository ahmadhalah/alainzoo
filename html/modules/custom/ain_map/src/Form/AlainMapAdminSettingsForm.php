<?php

namespace Drupal\ain_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure BsTour settings for this site.
 */
class AlainMapAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alain_map_form_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ain_map.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('ain_map.settings');

    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('General Configurations'),
      '#open' => TRUE,
    );

    $form['general']['map_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#multiple' => FALSE,
      '#default_value' => $config->get('accesstoken'),
    );

    $form['general']['map_style'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Map style'),
      '#multiple' => false,
      '#default_value' => $config->get('style'),
    );

    $form['general']['map_regions_dataset'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Regions dataset'),
      '#multiple' => false,
      '#default_value' => $config->get('dataset'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ain_map.settings');
    $config
      ->set('accesstoken', $form_state->getValue('map_access_token'))
      ->set('style', $form_state->getValue('map_style'))
      ->set('dataset', $form_state->getValue('map_regions_dataset'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.maps = {
    attach: function (context, settings) {
      var searchArray = new Array();
      var count = 0;
      $(document).ready(function () {

        var mapSettings = $(drupalSettings.ain_map.map)[0];
        mapboxgl.accessToken = mapSettings.accesstoken;

        var map = new mapboxgl.Map({
          container: 'alain-map',
          style: mapSettings.style,
          center: [55.740, 24.176],
          bearing: 90,
          zoom: 16.2,
          minZoom: 16
        });

        // Add zoom and rotation controls to the map.
        map.addControl(new mapboxgl.NavigationControl());
        // Add full screen controll.
        map.addControl(new mapboxgl.FullscreenControl());

        var markers = [];

        var markerFields = [
          ['field--name-field-latitude', 'field--name-field-longitude'],
          ['field--name-field-latitude2', 'field--name-field-longitude2'],
        ];

        markerFields.forEach(function (markerField, key) {
          var lat = $('.' + markerField[0] + ' input').val();
          var long = $('.' + markerField[1] + ' input').val();
          if (lat && long) {
            markers[key] = createMarker(lat, long);
          }
        });

        function createMarker(lat, long) {
          var icon = document.createElement('div');
          icon.className = 'marker';
          icon.style.backgroundImage = 'url(/themes/ain/images/map-marker-icon.png)';
          icon.style.width = 50 + 'px';
          icon.style.height = 50 + 'px';

          map.flyTo({center: [long, lat], zoom: 17.5, speed: 0.4, curve: 1});
          // Add marker to map
          return new mapboxgl.Marker(icon)
            .setLngLat([long, lat])
            .addTo(map);
        }

        map.on('click', function (point) {
          var lat = $('.field--name-field-latitude input').val();
          var long = $('.field--name-field-longitude input').val();

          if (lat && long) {
            if ($('.field--name-field-latitude2 input').length && $('.field--name-field-longitude2 input').length) {
              var lat2 = $('.field--name-field-latitude2 input').val();
              var long2 = $('.field--name-field-longitude2 input').val();

              if (lat2 && long2) {
                alert(Drupal.t('Please clear your location'));
              }
              else {
                $('.field--name-field-latitude2 input').val(point['lngLat'].lat);
                $('.field--name-field-longitude2 input').val(point['lngLat'].lng);
                createMarker(point['lngLat'].lat, point['lngLat'].lng);
              }
            } else {
              alert(Drupal.t('Please clear your location'));
            }
          }
          else {
            $('.field--name-field-latitude input').val(point['lngLat'].lat);
            $('.field--name-field-longitude input').val(point['lngLat'].lng);
            createMarker(point['lngLat'].lat, point['lngLat'].lng);
          }
        });
      });
    }
  };
}
)(window.jQuery, window._, window.Drupal, window.drupalSettings, window.localStorage);
